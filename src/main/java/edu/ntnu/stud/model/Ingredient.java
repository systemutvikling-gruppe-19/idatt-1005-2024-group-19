package edu.ntnu.stud.model;

/**
 * The Ingredient class represents an ingredient in a recipe.
 * Is also used in the Inventory class and the ShoppingList class.
 */
public class Ingredient {

  // Class variables
  private final String ingName;
  private int quantity;

  /**
   * Create a new Ingredient object with the given name and quantity.
   *
   * @param ingName The name of the ingredient
   * @param quantity The quantity of the ingredient
   */
  public Ingredient(String ingName, int quantity) {
    this.ingName = ingName;
    this.quantity = quantity;
  }

  /**
   * Get the name of the ingredient.
   *
   * @return The name of the ingredient
   */
  public String getIngName() {
    return ingName;
  }

  /**
   * Get the quantity of the ingredient.
   *
   * @return The quantity of the ingredient
   */
  public int getQuantity() {
    return quantity;
  }

  /**
   * Set the quantity of the ingredient.
   *
   * @param quantity The new quantity of the ingredient
   */
  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }
}