package edu.ntnu.stud.tests.model;

import edu.ntnu.stud.model.Ingredient;
import org.junit.jupiter.api.Test;

import java.util.List;

import static edu.ntnu.stud.model.ShoppingList.*;
import static org.junit.Assert.assertEquals;

public class ShoppingListTest {



    @Test
    void test_clearShoppingList() {
        clearShoppingList();
        List<Ingredient> shoppingList = getShoppingList();
        assertEquals(0, shoppingList.size());
    }
}