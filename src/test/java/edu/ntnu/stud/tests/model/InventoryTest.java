package edu.ntnu.stud.tests.model;

import edu.ntnu.stud.model.Ingredient;
import edu.ntnu.stud.model.Inventory;
import javafx.util.Pair;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static edu.ntnu.stud.model.Inventory.getInventory;
import static org.junit.jupiter.api.Assertions.*;

public class InventoryTest {

    @Test
    void testShowInventoryWithoutZero() {
        List<Ingredient> list = new ArrayList<>();
        list.add(new Ingredient("Sugar", 5));
        list.add(new Ingredient("Flour", 0));
        list.add(new Ingredient("Salt", 2));

        Collection<Pair<String, String>> result = Inventory.showInventoryWithoutZero(list);

        assertEquals(2, result.size());
    }
    @Test
    void negativeTestShowInventoryWithoutZero() {
        List<Ingredient> list = new ArrayList<>();
        list.add(new Ingredient("Sugar", 5));
        list.add(new Ingredient("Flour", 0));
        list.add(new Ingredient("Salt", 2));

        Collection<Pair<String, String>> result = Inventory.showInventoryWithoutZero(list);

        assertNotEquals(3, result.size());
    }
}
