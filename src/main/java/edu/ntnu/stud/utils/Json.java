package edu.ntnu.stud.utils;

import static edu.ntnu.stud.utils.InputValid.validateInt;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.ntnu.stud.model.Ingredient;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONTokener;

/**
 * Utility class for handling JSON files.
 */
public class Json {

  // Logger
  private static final Logger LOGGER = LoggerUtil.getLogger(Json.class);

  // Constants
  private static final String JSON_PATH = "src/main/resources/json/";
  private static final String JSON_EXTENSION = ".json";
  private static final String INGREDIENTS_NAME = "ing_name";
  private static final String QUANTITY_NAME = "quantity";

  /**
   * Reads a JSON file and returns a JSONArray.
   *
   * @param fileName the name of the JSON file to read.
   * @return a JSONArray containing the contents of the JSON file.
   * @throws FileNotFoundException if the file is not found.
   */
  public static JSONArray getJsonArray(String fileName) throws FileNotFoundException {
    String filepath = JSON_PATH + fileName + JSON_EXTENSION;
    // Create a FileReader object for the JSON file
    FileReader fileReader = new FileReader(filepath);

    // Create a JSONTokener to tokenize the FileReader
    JSONTokener jsonTokener = new JSONTokener(fileReader);

    // Create a JSONArray by parsing the JSONTokener
    return new JSONArray(jsonTokener);
  }

  /**
   * Reads a JSON file and populates a list with the ingredients.
   *
   * @param listName the list to populate with ingredients.
   * @param fileName the name of the JSON file to read.
   */
  public static void jsonToIngredientList(List<Ingredient> listName, String fileName) {
    String filepath = JSON_PATH + fileName + JSON_EXTENSION;
    ObjectMapper objectMapper = new ObjectMapper();

    try {
      // Load ingredients from json file
      JsonNode inventoryNode = objectMapper.readTree(new File(filepath));

      // Iterate over each ingredient node in the json file
      for (JsonNode ingredientNode : inventoryNode) {
        String ingName = ingredientNode.get(INGREDIENTS_NAME).asText();
        int ingQuantity = ingredientNode.get(QUANTITY_NAME).asInt();

        // Create an Ingredient object and add it to the list
        listName.add(new Ingredient(ingName, ingQuantity));
      }
    } catch (IOException e) {
      LOGGER.severe("Failed to read JSON file: " + e.getMessage());
    }
  }

  /**
   * Writes a list of ingredients to a JSON file.
   *
   * @param listName the list of ingredients to write to the JSON file.
   * @param fileName the name of the JSON file to write to.
   */
  public static void listToJson(List<Ingredient> listName, String fileName) {
    String filepath = JSON_PATH + fileName + JSON_EXTENSION;
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

    ArrayNode shoppingListArrayNode = objectMapper.createArrayNode();

    for (Ingredient ingredient : listName) {
      ObjectNode ingredientNode = objectMapper.createObjectNode();
      ingredientNode.put(INGREDIENTS_NAME, ingredient.getIngName());
      ingredientNode.put(QUANTITY_NAME, ingredient.getQuantity());
      shoppingListArrayNode.add(ingredientNode);
    }

    try {
      objectMapper.writeValue(new File(filepath), shoppingListArrayNode);
    } catch (IOException e) {
      LOGGER.severe("Failed to write JSON file: " + e.getMessage());
    }
  }

  /**
   * Adds an ingredient to a JSON file.
   *
   * @param fileName the name of the JSON file to add the ingredient to.
   * @param ingName the name of the ingredient to add.
   * @param quantity the quantity of the ingredient to add.
   * @throws IllegalArgumentException if the quantity is not a positive number.
   */
  public static void addIngredientToJson(String fileName, String ingName,
                                         String quantity) throws IllegalArgumentException {
    String filePath = JSON_PATH + fileName + JSON_EXTENSION;
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    boolean ingredientExists = false;
    validateInt(quantity);

    try {
      // Load ingredients from json file
      File file = new File(filePath);
      ArrayNode fileNodes = (ArrayNode) objectMapper.readTree(file);

      // Check if the ingredient already exists in the file, if so, update the quantity
      for (int i = 0; i < fileNodes.size(); i++) {
        ObjectNode ingredientNode = (ObjectNode) fileNodes.get(i);
        if (ingredientNode.get(INGREDIENTS_NAME).asText().trim().equalsIgnoreCase(ingName.trim())) {
          ingredientNode.put(QUANTITY_NAME, ingredientNode.get(QUANTITY_NAME).asInt()
              + Integer.parseInt(quantity));
          ingredientExists = true;
          break;
        }
      }

      // If the ingredient doesn't exist, add it to the fileNodes
      if (!ingredientExists) {
        // Create a new ingredient node and add it to the fileNodes
        ObjectNode newIngredientNode = objectMapper.createObjectNode();
        newIngredientNode.put(INGREDIENTS_NAME, ingName);
        newIngredientNode.put(QUANTITY_NAME, Integer.parseInt(quantity));
        fileNodes.add(newIngredientNode);
      }

      // Write the updated fileNodes back to the file
      objectMapper.writeValue(file, fileNodes);
    } catch (IOException e) {
      LOGGER.severe("Failed to write JSON file: " + e.getMessage());
    }
  }
}