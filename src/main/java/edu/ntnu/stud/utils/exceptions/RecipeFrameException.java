package edu.ntnu.stud.utils.exceptions;

/**
 * Exception thrown when there is an error in the RecipeFrame class.
 */
public class RecipeFrameException extends RuntimeException {

  /**
   * Constructs a new RecipeFrameException with the specified detail message and cause.
   *
   * @param message the detail message (which is saved for later retrieval
   *                by the getMessage() method).
   * @param cause the cause (which is saved for later retrieval by the getCause() method).
   */
  public RecipeFrameException(String message, Throwable cause) {
    super(message, cause);
  }
}