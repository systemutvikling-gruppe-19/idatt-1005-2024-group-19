package edu.ntnu.stud.model.database;

import edu.ntnu.stud.utils.LoggerUtil;
import edu.ntnu.stud.utils.exceptions.DbConnectionErrorException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * This class provides a connection to the database.
 * Mention being written by Surya.
 * We do have additions though: the getConnectionNoDB method was not in Surya's code.
 */
public class DbLink {

  //Logger
  private static final Logger LOGGER = LoggerUtil.getLogger(DbLink.class);

  // Database connection details
  private static final String IP_TO_DB_SERVER = "localhost"; // Assuming MySQL is running locally
  private static final String DB_USE_SSL = "?useSSL=false";
  private static final int PORT_NUMBER = 3306; // MySQL default port number

  // Singleton instance
  private static DbLink databaseConnectionProvider;

  // Database details
  private static final String DB_NAME = "quicklist"; // Your database name

  // Database connection details
  private final String url;
  private final String urlNoDB;
  private final String username;
  private final String password;

  /**
   * Constructor for the DbLink class. Creates a connection to the database.
   */
  public DbLink() {
    this.url = "jdbc:mysql://" + IP_TO_DB_SERVER + ":" + PORT_NUMBER + "/" + DB_NAME + DB_USE_SSL;
    this.urlNoDB = "jdbc:mysql://" + IP_TO_DB_SERVER + ":" + PORT_NUMBER + "/" + DB_USE_SSL;
    this.username = "root"; // Assuming you're using the root username
    this.password = ""; // Assuming you haven't set a password for root
  }

  /**
   * Constructor for the DbLink class.
   * Creates a connection to the database with the given parameters.
   *
   * @param url      The URL to the database.
   * @param username The username to the database.
   * @param password The password to the database.
   */
  public DbLink(String url, String urlNoDb, String username, String password) {
    this.url = url;
    this.urlNoDB = urlNoDb;
    this.username = username;
    this.password = password;
  }

  /**
   * Returns the instance of the database connection provider.
   *
   * @return The instance of the database connection provider.
   */
  public static DbLink instance() {
    if (databaseConnectionProvider == null) {
      databaseConnectionProvider = new DbLink();
    }
    return databaseConnectionProvider;
  }

  /**
   * Closes connections to database, makes sure that resultSets
   * and statements gets closed properly.
   *
   * @param connection        the connection to be closed
   * @param preparedStatement the preparedStatement to be closed
   * @param resultSet         the resultSet to be closed
   */
  public static void close(Connection connection, PreparedStatement preparedStatement,
                           ResultSet resultSet) {
    if (resultSet != null) {
      try {
        resultSet.close();
      } catch (SQLException e) {
        LOGGER.warning("Failed to close ResultSet");
      }
    }
    if (preparedStatement != null) {
      try {
        preparedStatement.close();
      } catch (SQLException e) {
        LOGGER.warning("Failed to close PreparedStatement");
      }
    }
    if (connection != null) {
      try {
        connection.close();
      } catch (SQLException e) {
        LOGGER.warning("Failed to close Connection");
      }
    }
  }

  /**
   * Returns a connection to the database.
   *
   * @return A connection to the database.
   * @throws DbConnectionErrorException If an error occurs while connecting to the database.
   */
  public Connection getConnection() throws DbConnectionErrorException {
    try {
      return DriverManager.getConnection(url, username, password);
    } catch (Exception e) {
      throw new DbConnectionErrorException(e.getMessage(), e);
    }
  }

  /**
   * Returns a connection to the database without specifying a database.
   *
   * @param dbName The name of the database to connect to.
   * @return A connection to the database.
   * @throws DbConnectionErrorException If an error occurs while connecting to the database.
   */
  public Connection getConnectionNoDB(String dbName) throws DbConnectionErrorException {
    String finalUrl = urlNoDB;
    if (!dbName.isEmpty()) {
      finalUrl += "/" + dbName;
    }
    try {
      return DriverManager.getConnection(finalUrl, username, password);
    } catch (Exception e) {
      throw new DbConnectionErrorException(e.getMessage(), e);
    }
  }
}

