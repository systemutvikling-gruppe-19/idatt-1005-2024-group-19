package edu.ntnu.stud.model;

import static edu.ntnu.stud.model.Inventory.getInventory;
import static edu.ntnu.stud.model.Inventory.reduceInventoryByRecipe;
import static edu.ntnu.stud.utils.Json.listToJson;

import edu.ntnu.stud.utils.Json;
import edu.ntnu.stud.utils.ListHandling;
import java.util.ArrayList;
import java.util.List;

/**
 * The ShoppingList class is responsible for managing the user's shopping list.
 */
public class ShoppingList {


  // List of ingredients in the shopping list
  private static List<Ingredient> shoppingList = new ArrayList<>();


  // Constant
  private static final String SHOPPING_LIST_STRING = "shopping_list";

  /**
   * Updates the shopping list by comparing the ingredients in the recipe with the inventory.
   *
   * @param recipeName The name of the recipe to update the shopping list with
   */
  public static void updateShoppingList(String recipeName) {
    List<Ingredient> newShoppingList = getShoppingList();
    ListHandling.addSubtractFromList(newShoppingList,
        compareExistingIngredientsFromRecipe(recipeName), 1);
    ListHandling.addSubtractFromList(newShoppingList,
        addNonExistingIngredientsFromRecipe(recipeName), 1);
    shoppingList = newShoppingList;

    listToJson(shoppingList, SHOPPING_LIST_STRING);
    reduceInventoryByRecipe(recipeName);
  }

  /**
   * Adds ingredients from a recipe to the shopping list that are not already in the inventory.
   *
   * @param recipeName The name of the recipe to add ingredients from
   * @return A list of ingredients to add to the shopping list
   */
  private static List<Ingredient> addNonExistingIngredientsFromRecipe(String recipeName) {
    List<Ingredient> inventory = getInventory();
    List<Ingredient> recipeIngredients = ListHandling.getIngredientsFromRecipe(recipeName);
    List<Ingredient> nonExistingIngredients = new ArrayList<>();

    // Iterate over ingredients in the recipe
    for (Ingredient recipeIngredient : recipeIngredients) {
      String recipeIngredientIngName = recipeIngredient.getIngName();
      int cartQuantity = recipeIngredient.getQuantity();
      boolean foundInInventory = false;

      // Check if the ingredient exists in the inventory
      for (Ingredient invIngredient : inventory) {
        String invIngredientName = invIngredient.getIngName();
        if (recipeIngredientIngName.equals(invIngredientName)) {
          foundInInventory = true;
          break;
        }
      }

      // If the ingredient is not found in the inventory, add it to the shopping list
      if (!foundInInventory) {
        nonExistingIngredients.add(new Ingredient(recipeIngredientIngName, cartQuantity));
      }
    }
    return nonExistingIngredients;
  }

  /**
   * Compares the ingredients in a recipe with the inventory and returns
   * a list of ingredients to add to the shopping list.
   *
   * @param recipeName The name of the recipe to compare ingredients with
   * @return A list of ingredients to add to the shopping list
   */
  private static List<Ingredient> compareExistingIngredientsFromRecipe(String recipeName) {
    List<Ingredient> inventory = getInventory();
    List<Ingredient> recipeIngredients = ListHandling.getIngredientsFromRecipe(recipeName);
    List<Ingredient> itemsToAddToShoppinglist = new ArrayList<>();

    // Iterate over inventory
    for (Ingredient invIngredient : inventory) {
      String invIngredientName = invIngredient.getIngName();
      int invQuantity = invIngredient.getQuantity();

      for (Ingredient recipeIngredient : recipeIngredients) {
        String recipeIngredientIngName = recipeIngredient.getIngName();
        int recipeQuantity = recipeIngredient.getQuantity();

        if (invIngredientName.equalsIgnoreCase(recipeIngredientIngName)) {
          int quantityComparison = invQuantity - recipeQuantity;


          if (quantityComparison < 0) {
            itemsToAddToShoppinglist.add(new Ingredient(invIngredientName,
                Math.abs(quantityComparison)));
          } else if (quantityComparison > 0) {
            //Reduce the quantity of the ingredient in the inventory

          }
        }
      }
    }
    return itemsToAddToShoppinglist;
  }

  /**
   * Get the shopping list from shopping_list.json.
   *
   * @return The shopping list
   */
  public static List<Ingredient> getShoppingList() {
    Json.jsonToIngredientList(shoppingList, SHOPPING_LIST_STRING);
    return shoppingList;
  }

  /**
   * Clears the shopping list by removing all items from the shopping list and shopping_list.json.
   */
  public static void clearShoppingList() {
    shoppingList.clear();
    listToJson(shoppingList, SHOPPING_LIST_STRING);
  }
}