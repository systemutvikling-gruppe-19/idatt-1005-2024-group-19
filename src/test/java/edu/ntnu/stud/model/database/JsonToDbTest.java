package edu.ntnu.stud.model.database;

import edu.ntnu.stud.utils.exceptions.DbConnectionErrorException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JsonToDbTest {

    @BeforeEach
    void setUp() {
        DbInit.initializeDatabase();
    }

//    @Test
//    void test_initRecipeFromJson() {
//        JsonToDb.initRecipeFromJson();
//    }

    @Test
    void test_updateInventory() throws DbConnectionErrorException {
        JsonToDb.updateInventoryFromJson();
    }

    @Test
    void test_updateShoppingList() {
        JsonToDb.updateFromJson("shopping_list");
    }
}