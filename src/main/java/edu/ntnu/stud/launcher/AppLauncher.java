package edu.ntnu.stud.launcher;

import edu.ntnu.stud.controller.MainApplication;

/**
 * The class responsible for launching the application.
 */
public class AppLauncher {

  /**
   * The method launches the application.
   *
   * @param args Arguments from user input.
   */
  public static void main(String[] args) {
    MainApplication.launcher(args);
  }
}
