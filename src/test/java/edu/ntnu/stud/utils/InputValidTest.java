package edu.ntnu.stud.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class InputValidTest {
    @Test
    void testValidateIntNegative() {
        assertThrows(IllegalArgumentException.class, () -> InputValid.validateInt("-1"));
    }

    @Test
    void testValidateIntZero() {
        assertThrows(IllegalArgumentException.class, () -> InputValid.validateInt("0"));
    }

    @Test
    void testValidateIntNonNumeric() {
        assertThrows(IllegalArgumentException.class, () -> InputValid.validateInt("abc"));
    }

    @Test
    void testValidateIntPositive() {
        InputValid.validateInt("1"); // Should not throw an exception
    }
}
