package edu.ntnu.stud.controller.framecontroller.dialogboxes;

import static edu.ntnu.stud.utils.Json.listToJson;

import edu.ntnu.stud.model.Ingredient;
import edu.ntnu.stud.utils.ListHandling;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Dialog box to display two input fields for getting a name and quantity
 * when an ingredient should be added to a list. The class extends the DialogBox class.
 *
 * @see DialogBox
 */
public class AddIngredientDialog extends DialogBox {

  /**
   * Constructor for the AddIngredientDialog class. Calls the constructor of the superclass.
   *
   * @param alert      The alert object to be displayed.
   * @param headerText The header text of the dialog box.
   * @param contentText The content text of the dialog box.
   * @param width      The width of the dialog box.
   * @param height     The height of the dialog box.
   */
  public AddIngredientDialog(Alert alert, Text headerText,
                             Text contentText, int width, int height) {
    super(alert, headerText, contentText, width, height);
    this.setStyle();
  }

  /**
   * Method to show the dialog box for adding an ingredient to a list.
   *
   * @param listName The list of ingredients to add the ingredient to.
   * @param fileName The name of the file to save the list to.
   */
  public void showAddIngredientDialog(List<Ingredient> listName, String fileName) {

    // Set the style of the dialog box
    DialogPane dialogPane = getAlert().getDialogPane();
    Button ok = (Button) dialogPane.lookupButton(ButtonType.OK);
    Button cancel = (Button) dialogPane.lookupButton(ButtonType.CANCEL);
    setPositiveButtonStyle(ok);
    setNegativeButtonStyle(cancel);

    // Create error labels for the input fields
    Label errorName = new Label();
    Label errorQuantity = new Label();
    errorName.setTextFill(javafx.scene.paint.Color.RED);
    errorQuantity.setTextFill(javafx.scene.paint.Color.RED);

    // Create the input fields for the name and quantity of the ingredient
    Text name = new Text("Name: ");
    Text quantity = new Text("Quantity: ");
    name.getStyleClass().add("textForTextFields");
    quantity.getStyleClass().add("textForTextFields");
    TextField nameField = new TextField();
    TextField quantityField = new TextField();

    // Create a grid pane to display the input fields and error labels
    GridPane gridPane = new GridPane();
    gridPane.add(name, 0, 0);
    gridPane.add(nameField, 1, 0);
    gridPane.add(quantity, 0, 2);
    gridPane.add(quantityField, 1, 2);
    gridPane.add(errorName, 1, 1);
    gridPane.add(errorQuantity, 1, 3);

    // Create a vertical box to display the header text and the grid pane
    VBox vbox = new VBox();
    vbox.getChildren().addAll(getHeaderText(), gridPane);
    vbox.setSpacing(20);

    // Set the content of the dialog box to the vertical box
    getAlert().getDialogPane().setContent(vbox);

    // Add an event filter to the OK button to add the ingredient to the list
    ok.addEventFilter(ActionEvent.ACTION, event -> {
      try {
        ListHandling.addIngredientToList(listName, nameField.getText(), quantityField.getText());
        listToJson(listName, fileName);
      } catch (IllegalArgumentException e) {
        errorQuantity.setText(e.getMessage());
        event.consume();
      }
    });
    getAlert().showAndWait();
  }
}
