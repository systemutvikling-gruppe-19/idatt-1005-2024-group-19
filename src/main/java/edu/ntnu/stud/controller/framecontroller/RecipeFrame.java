package edu.ntnu.stud.controller.framecontroller;

import edu.ntnu.stud.utils.LoggerUtil;
import java.io.IOException;
import java.util.Collection;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Pair;

/**
 * A frame that displays a recipe with its name, steps, image and ingredients.
 * The frame also contains a button to add the recipe to the shopping list.
 */
public class RecipeFrame {

  // Logger
  private static final Logger LOGGER = LoggerUtil.getLogger(RecipeFrame.class);

  // Frames
  private StackPane frame;
  private final Ingredients ingredientsFrame;

  // FXML elements
  @FXML
  @SuppressWarnings("unused")
  private Button addToShoppingListButton;
  @FXML
  @SuppressWarnings("unused")
  private VBox imageBox;
  @FXML
  @SuppressWarnings("unused")
  private Text recipeTitle;
  @FXML
  @SuppressWarnings("unused")
  private Text recipeSteps;
  @FXML
  @SuppressWarnings("unused")
  private StackPane ingredientsBox;

  /**
   * Constructor for the RecipeFrame class. Loads the fxml file and initializes
   * the ingredients frame. A new instance of an IngredientsFrame is created
   * to be able to use the ItemListFrame class' methods.
   */
  public RecipeFrame() {
    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/recipeFrame.fxml"));
      loader.setController(this);
      frame = loader.load();
    } catch (IOException e) {
      LOGGER.severe("Failed to load recipeFrame.fxml");
    }
    ingredientsFrame = new Ingredients();
  }

  /**
   * Sets the content of the recipe frame with the given name, steps, image and ingredients.
   *
   * @param name        the name of the recipe
   * @param steps       the steps of the recipe
   * @param imagePath   the path to the image of the recipe
   * @param ingredients the ingredients of the recipe
   */
  public void setContent(String name, String steps, String imagePath,
                         Collection<Pair<String, String>> ingredients) {
    recipeTitle.setText(name);
    recipeSteps.setText(steps);

    imageBox.getChildren().clear();
    imageBox.getChildren().add(createImageView(imagePath));

    ingredientsFrame.updateItems(ingredients);
    ingredientsBox.getChildren().add(ingredientsFrame.getFrame());
  }

  /**
   * Returns the frame of the recipe.
   *
   * @return the frame of the recipe
   */
  public StackPane getRecipeFrame() {
    return frame;
  }

  /**
   * Returns the button to add the recipe to the shopping list.
   *
   * @return the button to add the recipe to the shopping list
   */
  public Button getAddtoShoppingListButton() {
    return addToShoppingListButton;
  }

  /**
   * Creates an image view with the given image path.
   * The image view is clipped to a rectangle with rounded corners.
   *
   * @param imagePath the path to the image
   * @return the image view
   */
  private Pane createImageView(String imagePath) {
    Image image = new Image(imagePath);
    ImageView imageView = new ImageView(image);

    imageView.setPreserveRatio(true);
    double width = image.getWidth();
    double height = image.getHeight();

    if (height < width * 300 / 350) {
      imageView.setFitHeight(300);
    } else {
      imageView.setFitWidth(350);
    }

    Rectangle clip = new Rectangle(350, 300);
    clip.setArcWidth(35);
    clip.setArcHeight(35);

    StackPane pane = new StackPane(imageView);
    pane.setClip(clip);
    pane.setMaxSize(350, 300);

    imageView.setStyle("-fx-effect: innershadow(gaussian, rgba(0,0,0,0.3), 20, 0.1, 5, 10)");

    return pane;
  }

  /**
   * A private class that extends the ItemListFrame class.
   * This class is used to display the ingredients of a recipe.
   */
  private static class Ingredients extends ItemListFrame {

    /**
     * Constructor for the Ingredients class. Calls the constructor of the
     * ItemListFrame class with the path to the fxml fil that contains the ingredients box.
     */
    public Ingredients() {
      super("/fxml/recipeIngredientsBox.fxml");
    }
  }
}