package edu.ntnu.stud.utils;

import java.util.logging.Logger;

/**
 * Utility class for getting a logger for a class.
 */
public class LoggerUtil {

  /**
   * Get a logger for a class.
   *
   * @param clazz The class to get a logger for.
   * @return A logger for the class.
   */
  public static Logger getLogger(Class<?> clazz) {
    return Logger.getLogger(clazz.getName());
  }
}