package edu.ntnu.stud.controller.framecontroller;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import javafx.util.Pair;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * An interface for reading JSON objects.
 */
public interface JsonReadable {

  /**
   * Reads a list of items from a JSON array.
   *
   * @param jsonArray  The JSON array to read from.
   * @return  A collection of pairs of strings.
   * @throws FileNotFoundException  If the file is not found.
   */
  default Collection<Pair<String, String>> readListFromJson(JSONArray jsonArray)
      throws FileNotFoundException {

    Collection<Pair<String, String>> items = new ArrayList<>();
    int i;

    for (i = 0; i < jsonArray.length(); ) {
      JSONObject jsonObject = jsonArray.getJSONObject(i);
      items.add(new Pair<>(jsonObject.getString("ing_name"),
          String.valueOf(jsonObject.getInt("quantity"))));
      i++;
    }
    return items;
  }
}