package edu.ntnu.stud.controller.framecontroller;

import edu.ntnu.stud.utils.LoggerUtil;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Pair;

/**
 * An abstract class which represents a basic table frame used for the Inventory and Shopping List.
 * The class implements the Initializable interface and provides a method for updating the items in
 * the table.
 *
 * @see javafx.fxml.Initializable The Initializable interface is used to ensure the fxml file is
 *        loaded correctly.
 */
public abstract class ItemListFrame implements Initializable {

  // Class variables //
  private static final Logger LOGGER = LoggerUtil.getLogger(ItemListFrame.class);
  @FXML
  protected VBox itemBox;

  @FXML
  protected TableView<Pair<String, String>> tableView;

  @FXML
  protected TableColumn<Pair<String, String>, String> nameColumn;

  @FXML
  protected TableColumn<Pair<String, String>, String> amountColumn;

  protected StackPane frame;

  /**
   * Takes in the file name for the .fxml file for the class extending the ItemListFrame class.
   *
   * @param fxmlFilePath  The file path to the .fxml file.
   */
  protected ItemListFrame(String fxmlFilePath) {
    FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlFilePath));
    loader.setController(this);
    try {
      frame = loader.load();
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, "Failed to load fxml file", e);
    }
  }

  /**
   * Initializes the table with the columns and sets the items in the table. Implemented from the
   * Initializable interface.
   *
   * @param location    The location used to resolve relative paths for the root object,
   *                    or null if the location is not known.
   * @param resources   The resources used to localize the root object,
   *                    or null if the root object was not localized.
   */
  public void initialize(URL location, ResourceBundle resources) {
    // Set up the columns in the table
    nameColumn.setCellValueFactory(cellData ->
      new SimpleStringProperty(cellData.getValue().getKey()));
    amountColumn.setCellValueFactory(cellData ->
      new SimpleStringProperty(cellData.getValue().getValue()));

    amountColumn.setCellFactory(tc -> new TableCell<>() {
      @Override
      protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        setGraphic(null);
        setText(empty ? null : item);
        setAlignment(Pos.CENTER_RIGHT);
      }
    });
    // Disable column reordering and resizing
    nameColumn.setReorderable(false);
    amountColumn.setReorderable(false);
    nameColumn.setResizable(false);
    amountColumn.setResizable(false);

    // Set the columns to take up 80% and 20% of the TableView's width
    nameColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.8));
    amountColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(0.2));

    // Set the column resize policy to constrained resize policy
    tableView.setColumnResizePolicy(param -> Boolean.TRUE);

    // Bind the TableView's maxWidth to the width of its parent VBox
    tableView.maxWidthProperty().bind(itemBox.widthProperty());
  }

  /**
   * Updates the items in the table.
   *
   * @param items  The items to be updated in the table.
   */
  public void updateItems(Collection<Pair<String, String>> items) {
    // Clear existing items
    tableView.getItems().clear();

    // Add new items
    tableView.getItems().addAll(items);
  }

  /**
   * Returns the frame of the ItemListFrame.
   *
   * @return  The frame of the ItemListFrame.
   */
  public StackPane getFrame() {
    return frame;
  }
}