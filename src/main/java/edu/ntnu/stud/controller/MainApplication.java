package edu.ntnu.stud.controller;

import static edu.ntnu.stud.model.Inventory.getInventory;
import static edu.ntnu.stud.model.Inventory.showInventoryWithoutZero;
import static edu.ntnu.stud.model.ShoppingList.getShoppingList;
import static edu.ntnu.stud.model.database.DbInit.initializeDatabase;
import static edu.ntnu.stud.model.database.JsonToDb.initRecipeFromJson;
import static edu.ntnu.stud.utils.Json.getJsonArray;

import edu.ntnu.stud.controller.framecontroller.CookBookFrame;
import edu.ntnu.stud.controller.framecontroller.Header;
import edu.ntnu.stud.controller.framecontroller.InventoryFrame;
import edu.ntnu.stud.controller.framecontroller.RecipeFrame;
import edu.ntnu.stud.controller.framecontroller.ShoppingListFrame;
import edu.ntnu.stud.controller.framecontroller.dialogboxes.AddIngredientDialog;
import edu.ntnu.stud.controller.framecontroller.dialogboxes.ConfirmAddRecipeDialog;
import edu.ntnu.stud.controller.framecontroller.dialogboxes.ConfirmDeleteDialog;
import edu.ntnu.stud.controller.framecontroller.dialogboxes.ConfirmGetDialog;
import edu.ntnu.stud.controller.framecontroller.dialogboxes.ConfirmSaveDialog;
import edu.ntnu.stud.controller.framecontroller.dialogboxes.ErrorDialog;
import edu.ntnu.stud.utils.exceptions.DbConnectionErrorException;
import edu.ntnu.stud.utils.exceptions.UpdateItemException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.json.JSONArray;

/**
 * The main application class for the QuickList application.
 */
public class MainApplication extends Application {

  // Constants
  private static final String FAILED_TO_UPDATE_ITEMS_SHOPPING_LIST =
      "Failed to update items to shopping list";
  private static final String FAILED_TO_UPDATE_ITEMS_INVENTORY =
      "Failed to update items to inventory";
  private static final String STRING_SHOPPING_LIST = "shopping_list";
  private static final String STRING_INVENTORY = "inventory";
  private static final String SAVE_SHOPPING_LIST = """
          Do you want to save the shopping list to remote storage?
          This will overwrite currently saved list.
          
          The list is always saved locally
          """;
  private static final String SAVE_INVENTORY = """
          Do you want to save the inventory to remote storage?
          This will overwrite currently saved inventory.
          
          The inventory is always saved locally
          """;
  private static final String LOAD_SHOPPING_LIST = """
          Do you want to load the shopping list from remote storage?
          This will overwrite the local shopping list.
          """;
  private static final String LOAD_INVENTORY = """
          Do you want to load the inventory from remote storage?
          This will overwrite the local inventory.
          """;

  // Class Fields
  private VBox vbox;
  private Header header;
  private ToggleButton cookBookButton;
  private ToggleButton inventoryButton;
  private ToggleButton shoppingListButton;

  /**
   * The main method to launch the application.
   *
   * @param args Arguments from user input.
   */
  public static void launcher(String[] args) {
    launch(args);
  }

  /**
   * The start method for the application and set up the components.
   *
   * @param primaryStage The primary stage for the application.
   * @throws IOException If an error occurs while reading the JSON file.
   */
  @Override
  public void start(Stage primaryStage) throws IOException {

    primaryStage.setTitle("QuickList");
    primaryStage.setMaximized(true);
    primaryStage.getIcons().add(new Image(Objects.requireNonNull(
        getClass().getResourceAsStream("/logo.png"))));

    header = new Header();
    cookBookButton = (ToggleButton) header.getCookBookButton().getChildren().getFirst();
    inventoryButton = (ToggleButton) header.getInventoryButton().getChildren().getFirst();
    shoppingListButton = (ToggleButton) header.getShoppingListButton().getChildren().getFirst();

    CookBookFrame cookBookFrame = new CookBookFrame(this);
    InventoryFrame inventoryFrame = new InventoryFrame();
    ShoppingListFrame shoppingListFrame = new ShoppingListFrame();

    cookBookButton.setOnAction(e ->
        showCookBookFrame(vbox, cookBookFrame));
    inventoryButton.setOnAction(e ->
        showInventoryFrame(vbox, inventoryFrame, showInventoryWithoutZero(getInventory())));
    shoppingListButton.setOnAction(e -> {
      try {
        showShoppingListFrame(vbox, shoppingListFrame, shoppingListFrame.readListFromJson(
            getJsonArray(STRING_SHOPPING_LIST)));
      } catch (FileNotFoundException ex) {
        throw new UpdateItemException(FAILED_TO_UPDATE_ITEMS_SHOPPING_LIST, ex);
      }
    });

    // Set the cookbook image from JSON
    cookBookFrame.setImageFromJson("src/main/resources/json/cookbook.json");
    // Set the cookbook button as selected at startup
    cookBookButton.setSelected(true);

    vbox = new VBox();
    vbox.setSpacing(30);
    vbox.getStyleClass().add("background");
    vbox.getChildren().addAll(header.getHeader(), cookBookFrame.getCookBookFrame());

    // Actions for shopping list frame
    shoppingListFrame.setShoppingClearButtonAction(e -> {
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
      Text headerText = new Text("Delete shopping list");
      Text contentText = new Text("Which shopping list do you want to delete?");
      // Create confirmDeleteDialog
      ConfirmDeleteDialog confirmDeleteDialog = new ConfirmDeleteDialog(
          alert, headerText, contentText, 500, 300);

      confirmDeleteDialog.showDeleteDialog(STRING_SHOPPING_LIST);

      try {
        shoppingListFrame.updateItems(shoppingListFrame.readListFromJson(
            getJsonArray(STRING_SHOPPING_LIST)));
      } catch (FileNotFoundException ex) {
        throw new UpdateItemException(FAILED_TO_UPDATE_ITEMS_SHOPPING_LIST, ex);
      }
    });

    shoppingListFrame.setShoppingListToDbButtonAction(e -> {
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
      Text headerText = new Text("Save remotely");
      Text contentText = new Text(SAVE_SHOPPING_LIST);

      // Create confirmDeleteDialog
      ConfirmSaveDialog confirmSaveDialog = new ConfirmSaveDialog(
          alert, headerText, contentText, 500, 300);
      confirmSaveDialog.showSaveDialog(STRING_SHOPPING_LIST);

      try {
        shoppingListFrame.updateItems(shoppingListFrame.readListFromJson(
            getJsonArray(STRING_SHOPPING_LIST)));
      } catch (FileNotFoundException ex) {
        throw new UpdateItemException(FAILED_TO_UPDATE_ITEMS_SHOPPING_LIST, ex);
      }
    });

    shoppingListFrame.setAddIngredientButtonAction(e -> {
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
      Text headerText = new Text("Add Ingredient");
      Text contentText = new Text("filler text");

      //Create addIngredientDialog
      AddIngredientDialog addIngredientDialog = new AddIngredientDialog(
          alert, headerText, contentText, 500, 300);
      addIngredientDialog.showAddIngredientDialog(getShoppingList(), STRING_SHOPPING_LIST);

      try {
        shoppingListFrame.updateItems(inventoryFrame.readListFromJson(
            getJsonArray(STRING_SHOPPING_LIST)));
      } catch (FileNotFoundException ex) {
        throw new UpdateItemException(FAILED_TO_UPDATE_ITEMS_SHOPPING_LIST, ex);
      }
    });

    shoppingListFrame.setShoppingListGetButtonAction(e -> {
      try {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        Text headerText = new Text("Load shopping list");
        Text contentText = new Text(LOAD_SHOPPING_LIST);

        // Create confirmGetDialog
        ConfirmGetDialog confirmGetDialog = new ConfirmGetDialog(
            alert, headerText, contentText, 500, 300);
        confirmGetDialog.showGetDialog(STRING_SHOPPING_LIST);
        shoppingListFrame.updateItems(shoppingListFrame.readListFromJson(
            getJsonArray(STRING_SHOPPING_LIST)));
      } catch (FileNotFoundException | DbConnectionErrorException ex) {
        throw new RuntimeException(ex);
      }
    });

    // Actions for inventory frame
    inventoryFrame.inventoryClearButtonAction(e -> {
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
      Text headerText = new Text("Delete inventory");
      Text contentText = new Text("Which inventory do you want to delete?");

      // Create confirmDeleteDialog
      ConfirmDeleteDialog confirmDeleteDialog = new ConfirmDeleteDialog(
          alert, headerText, contentText, 500, 300);
      confirmDeleteDialog.showDeleteDialog(STRING_INVENTORY);

      try {
        inventoryFrame.updateItems(inventoryFrame.readListFromJson(
            getJsonArray(STRING_INVENTORY)));
      } catch (FileNotFoundException ex) {
        throw new UpdateItemException(FAILED_TO_UPDATE_ITEMS_INVENTORY, ex);
      }
    });

    inventoryFrame.inventoryToDbButtonAction(e -> {
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
      Text headerText = new Text("Save remotely");
      Text contentText = new Text(SAVE_INVENTORY);

      // Create confirmDeleteDialog
      ConfirmSaveDialog confirmSaveDialog = new ConfirmSaveDialog(
          alert, headerText, contentText, 500, 300);
      confirmSaveDialog.showSaveDialog(STRING_INVENTORY);

      try {
        inventoryFrame.updateItems(inventoryFrame.readListFromJson(
            getJsonArray(STRING_INVENTORY)));
      } catch (FileNotFoundException ex) {
        throw new UpdateItemException(FAILED_TO_UPDATE_ITEMS_INVENTORY, ex);
      }
    });

    inventoryFrame.setAddIngredientButtonAction(e -> {
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
      Text headerText = new Text("Add Ingredient");
      Text contentText = new Text("filler text");
      // Create addIngredientDialog
      AddIngredientDialog addIngredientDialog = new AddIngredientDialog(
          alert, headerText, contentText, 500, 300);

      addIngredientDialog.showAddIngredientDialog(getInventory(), STRING_INVENTORY);
      showInventoryFrame(vbox, inventoryFrame, showInventoryWithoutZero(getInventory()));
    });

    inventoryFrame.inventoryGetButtonAction(e -> {
      try {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        Text headerText = new Text("Load inventory");
        Text contentText = new Text(LOAD_INVENTORY);

        // Create confirmGetDialog
        ConfirmGetDialog confirmGetDialog = new ConfirmGetDialog(
            alert, headerText, contentText, 500, 300);
        confirmGetDialog.showGetDialog(STRING_INVENTORY);
        inventoryFrame.updateItems(inventoryFrame.readListFromJson(
            getJsonArray(STRING_INVENTORY)));
      } catch (FileNotFoundException | DbConnectionErrorException ex) {
        throw new RuntimeException(ex);
      }
    });

    // Set up the scene
    Scene scene = new Scene(vbox);
    scene.getStylesheets().add(Objects.requireNonNull(
        getClass().getResource("/globals.css")).toExternalForm());

    // Display the stage
    primaryStage.setScene(scene);
    primaryStage.show();

    //Alert if unable to connect to remote files
    try {
      initializeDatabase();
      initRecipeFromJson();
    } catch (IllegalArgumentException e) {
      Alert alert = new Alert(Alert.AlertType.ERROR);
      Text headerText = new Text(e.getMessage());
      Text contentText = new Text("Unable to connect to remote files.\n"
          + "Some functions may not work");
      //create ErrorDialog
      ErrorDialog errorDialog = new ErrorDialog(alert, headerText, contentText, 500, 300);
      errorDialog.showErrorDialog();

    }
  }

  /**
   * Shows the cookbook frame.
   *
   * @param vbox The VBox containing the frames.
   * @param cookBookFrame The cookbook frame.
   */
  public void showCookBookFrame(VBox vbox, CookBookFrame cookBookFrame) {
    vbox.getChildren().remove(1);
    vbox.getChildren().add(cookBookFrame.getCookBookFrame());
    header.clearCookBookNotificationCount();
    cookBookButton.setSelected(true);
    inventoryButton.setSelected(false);
    shoppingListButton.setSelected(false);
  }

  /**
   * Shows the inventory frame.
   *
   * @param vbox The VBox containing the frames.
   * @param inventoryFrame The inventory frame.
   * @param items The items to display in the inventory.
   */
  private void showInventoryFrame(VBox vbox, InventoryFrame inventoryFrame,
                                  Collection<Pair<String, String>> items) {
    vbox.getChildren().remove(1);
    inventoryFrame.updateItems(items);
    vbox.getChildren().add(inventoryFrame.getFrame());
    header.clearInventoryNotificationCount();
    cookBookButton.setSelected(false);
    inventoryButton.setSelected(true);
    shoppingListButton.setSelected(false);
  }

  /**
   * Shows the shopping list frame.
   *
   * @param vbox The VBox containing the frames.
   * @param shoppingListFrame The shopping list frame.
   * @param items The items to display in the shopping list.
   */
  private void showShoppingListFrame(VBox vbox, ShoppingListFrame shoppingListFrame,
                                     Collection<Pair<String, String>> items) {
    vbox.getChildren().remove(1);
    shoppingListFrame.updateItems(items);
    vbox.getChildren().add(shoppingListFrame.getFrame());
    header.clearShoppingListNotificationCount();
    cookBookButton.setSelected(false);
    inventoryButton.setSelected(false);
    shoppingListButton.setSelected(true);
    getShoppingList();
  }

  /**
   * Shows the recipe frame for the selected recipe.
   *
   * @param jsonIndex The index of the selected recipe in the JSON file.
   * @param jsonArray The JSON array containing the recipes.
   */
  public void showRecipeFrame(int jsonIndex, JSONArray jsonArray) {
    vbox.getChildren().remove(1);

    // Get recipe information from JSON
    String recipeName = jsonArray.getJSONObject(jsonIndex).getString("rec_name");
    String steps = jsonArray.getJSONObject(jsonIndex).getString("instruction");
    String image = jsonArray.getJSONObject(jsonIndex).getString("im_path");
    Collection<Pair<String, String>> ingredients = new ArrayList<>();
    JSONArray jsonIngredientsArray = jsonArray.getJSONObject(jsonIndex).getJSONArray("ingredients");
    for (int i = 0; i < jsonIngredientsArray.length(); i++) {
      ingredients.add(new Pair<>(
          jsonIngredientsArray.getJSONObject(i).getString("ing_name"),
          String.valueOf(jsonIngredientsArray.getJSONObject(i).getInt("quantity"))));
    }

    // Create and set content for the recipe frame
    RecipeFrame recipeFrame = new RecipeFrame();
    recipeFrame.setContent(recipeName, steps, image, ingredients);

    // Add the recipe frame to the Home page
    vbox.getChildren().add(recipeFrame.getRecipeFrame());
    cookBookButton.setSelected(false);
    inventoryButton.setSelected(false);
    shoppingListButton.setSelected(false);

    //Action for add to shopping list button from "More" pane
    recipeFrame.getAddtoShoppingListButton().setOnAction(e -> {
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
      Text headerText = new Text("Add recipe to shopping list");
      Text contentText = new Text("Do you want to add this entire recipe to the shopping list \n"
          + "or just the ingredients you dont have in the inventory?");
      // Create confirmAddRecipeDialog
      ConfirmAddRecipeDialog confirmAddRecipeDialog = new ConfirmAddRecipeDialog(
          alert, headerText, contentText, 700, 300, header);
      confirmAddRecipeDialog.showAddRecipeChoiceDialog(recipeName);

    });
  }

  /**
   * Returns the header of the application.
   *
   * @return The header of the application.
   */
  public Header getHeader() {
    return header;
  }
}