package edu.ntnu.stud.utils;

import edu.ntnu.stud.model.Ingredient;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

 class ListHandlingTest {
    @Test
    void testAddFromList() {
        List<Ingredient> mainList = new ArrayList<>();
        mainList.add(new Ingredient("Sugar", 5));
        mainList.add(new Ingredient("Flour", 3));

        List<Ingredient> subList = new ArrayList<>();
        subList.add(new Ingredient("Sugar", 2));
        subList.add(new Ingredient("Salt", 1));

        List<Ingredient> result = ListHandling.addSubtractFromList(mainList, subList, 1);

        assertEquals(3, result.size());
        assertEquals(7, result.get(0).getQuantity());
        assertEquals(3, result.get(1).getQuantity());
        assertEquals(1, result.get(2).getQuantity());
    }

     @Test
     void testSubtractFromList() {
         List<Ingredient> mainList = new ArrayList<>();
         mainList.add(new Ingredient("Sugar", 5));
         mainList.add(new Ingredient("Flour", 3));

         List<Ingredient> subList = new ArrayList<>();
         subList.add(new Ingredient("Sugar", 2));
         subList.add(new Ingredient("Salt", 1));

         List<Ingredient> result = ListHandling.addSubtractFromList(mainList, subList, -1);

         assertEquals(3, result.size());
         assertEquals(3, result.get(0).getQuantity());
         assertEquals(3, result.get(1).getQuantity());
         assertEquals(1, result.get(2).getQuantity());
     }
    @Test
    public void testAddIngredientToList() {
        List<Ingredient> list = new ArrayList<>();
        list.add(new Ingredient("Sugar", 5));
        list.add(new Ingredient("Flour", 3));

        ListHandling.addIngredientToList(list, "Salt", "2");

        assertEquals(3, list.size());
        assertEquals("Salt", list.get(2).getIngName());
        assertEquals(2, list.get(2).getQuantity());
    }


}
