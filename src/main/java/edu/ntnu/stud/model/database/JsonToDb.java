package edu.ntnu.stud.model.database;

import static edu.ntnu.stud.utils.Json.listToJson;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.ntnu.stud.model.Ingredient;
import edu.ntnu.stud.utils.LoggerUtil;
import edu.ntnu.stud.utils.exceptions.DbConnectionErrorException;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

/** This class is responsible for seeding the database with data from JSON files. */
public class JsonToDb {

  private static final Logger LOGGER = LoggerUtil.getLogger(JsonToDb.class);

  // Constants
  private static final String JSON_PATH = "src/main/resources/json/";
  private static final String JSON_EXTENSION = ".json";
  private static final String INGREDIENTS_NAME = "ing_name";
  private static final String QUANTITY_NAME = "quantity";
  private static final String INVENTORY_STRING = "inventory";

  private static final String SHOPPING_LIST_STRING = "shopping_list";

  /**
   * Populates the cookbook and recipeIngredient tables from a Json file.
   *
   * @see #insertRecipe(JsonNode)
   * @see #insertRecipeIngredients(JsonNode)
   */
  public static void initRecipeFromJson() {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      File jsonFile = new File(JSON_PATH + "cookbook" + JSON_EXTENSION);
      JsonNode rootNode = objectMapper.readTree(jsonFile);

      for (JsonNode recipeNode : rootNode) {
        insertRecipe(recipeNode);
        insertRecipeIngredients(recipeNode);
      }

    } catch (IOException | SQLException | DbConnectionErrorException e) {
      LOGGER.severe("Error while initializing recipe from JSON: " + e.getMessage());
    }
  }

  /**
   * Checks if a recipe already exists in the database.
   *
   * @param recipeName name of the recipe.
   * @throws SQLException If an error occurs while executing the SQL query.
   * @throws DbConnectionErrorException If an error occurs while connecting to the database.
   */
  private static boolean recipeExists(String recipeName)
      throws SQLException, DbConnectionErrorException {

    try (Connection connection = DbLink.instance().getConnection();
        PreparedStatement prepState =
            connection.prepareStatement("SELECT COUNT(*) FROM cookbook WHERE rec_name = ?")) {
      prepState.setString(1, recipeName);
      try (ResultSet resultSet = prepState.executeQuery()) {
        if (resultSet.next()) {
          int count = resultSet.getInt(1);
          return count > 0;
        }
      }
    }
    return false;
  }

  /**
   * Inserts a recipe into the cookbook table after checking if it already exists.
   *
   * @param recipeNode The recipe node to insert.
   * @throws SQLException If an error occurs while executing the SQL query.
   * @throws DbConnectionErrorException If an error occurs while connecting to the database.
   */
  private static void insertRecipe(JsonNode recipeNode)
      throws SQLException, DbConnectionErrorException {
    String recipeName = recipeNode.get("rec_name").asText();

    // Check if the recipe already exists
    if (recipeExists(recipeName)) {
      return; // Recipe already exists, skip to the next recipe
    }

    try (Connection connection = DbLink.instance().getConnection();
        PreparedStatement prepState =
            connection.prepareStatement(
                "INSERT INTO cookbook (rec_name, instruction, im_path) VALUES (?, ?, ?)")) {

      prepState.setString(1, recipeName);

      JsonNode instructionNode = recipeNode.get("instruction");
      if (instructionNode != null) {
        prepState.setString(2, instructionNode.asText());
      } else {
        prepState.setNull(2, java.sql.Types.VARCHAR);
      }

      JsonNode imPathNode = recipeNode.get("im_path");
      if (imPathNode != null) {
        prepState.setString(3, imPathNode.asText());
      } else {
        prepState.setNull(3, java.sql.Types.VARCHAR);
      }

      prepState.executeUpdate();
    }
  }

  /**
   * Inserts the ingredients of a recipe into the database after checking if they already exists.
   *
   * @param recipeNode The recipe node to insert.
   * @throws SQLException If an error occurs while executing the SQL query.
   * @throws DbConnectionErrorException If an error occurs while connecting to the database.
   */
  private static void insertRecipeIngredients(JsonNode recipeNode)
      throws DbConnectionErrorException, SQLException {
    String recipeName = recipeNode.get("rec_name").asText();
    JsonNode ingredientsNode = recipeNode.get("ingredients");

    try (Connection connection = DbLink.instance().getConnection()) {
      // Prepare a statement to check if the ingredient already exists for this recipe
      // Prepare a statement to insert the ingredient
      try (PreparedStatement checkIfExistsStmt =
              connection.prepareStatement(
                  "SELECT COUNT(*) FROM recipe_ingredients WHERE rec_name = ? AND ing_name = ?");
          PreparedStatement insertIngredientStmt =
              connection.prepareStatement(
                  "INSERT INTO recipe_ingredients (rec_name, ing_name, quantity) VALUES (?, ?, ?)")) {

        Iterator<JsonNode> iterator = ingredientsNode.elements();
        while (iterator.hasNext()) {
          JsonNode ingredientNode = iterator.next();
          String ingredientName = ingredientNode.get(INGREDIENTS_NAME).asText();
          int quantity = ingredientNode.get(QUANTITY_NAME).asInt();

          // Check if the ingredient already exists for this recipe
          checkIfExistsStmt.setString(1, recipeName);
          checkIfExistsStmt.setString(2, ingredientName);
          try (ResultSet resultSet = checkIfExistsStmt.executeQuery()) {
            resultSet.next();
            int count = resultSet.getInt(1);
            if (count > 0) {
              // Ingredient already exists for this recipe
              continue; // Skip to the next ingredient
            }
          }

          // Insert the ingredient
          insertIngredientStmt.setString(1, recipeName);
          insertIngredientStmt.setString(2, ingredientName);
          insertIngredientStmt.setInt(3, quantity);
          insertIngredientStmt.executeUpdate();
        }
      }
    }
  }

  /**
   * Updates the inventory table in the database with data from a JSON file.
   *
   * @throws DbConnectionErrorException If an error occurs while connecting to the database.
   */
  public static void updateInventoryFromJson() throws DbConnectionErrorException {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      File jsonFile = new File(JSON_PATH + INVENTORY_STRING + JSON_EXTENSION);
      JsonNode rootNode = objectMapper.readTree(jsonFile);

      for (JsonNode inventoryNode : rootNode) {
        insertInventory(inventoryNode);
      }

    } catch (IOException | SQLException e) {
      LOGGER.severe("Error while updating inventory from JSON: " + e.getMessage());
    }
  }

  /**
   * Inserts an inventory item into the corresponding table while checking if ingredient already
   * exists.
   *
   * @param inventoryNode The inventory node to insert.
   * @throws SQLException If an error occurs while executing the SQL query.
   * @throws DbConnectionErrorException If an error occurs while connecting to the database.
   */
  private static void insertInventory(JsonNode inventoryNode)
      throws SQLException, DbConnectionErrorException {
    try (Connection connection = DbLink.instance().getConnection()) {
      int count;
      // Check if the ingredient already exists in the database
      try (PreparedStatement checkIfExists =
          connection.prepareStatement("SELECT COUNT(*) FROM inventory WHERE ing_name = ?")) {
        checkIfExists.setString(1, inventoryNode.get(INGREDIENTS_NAME).asText());
        ResultSet resultSet = checkIfExists.executeQuery();
        resultSet.next();
        count = resultSet.getInt(1);
        resultSet.close();
      }

      if (count > 0) {
        // Ingredient already exists, update the quantity
        try (PreparedStatement updateStatement =
            connection.prepareStatement("UPDATE inventory SET quantity = ? WHERE ing_name = ?")) {
          updateStatement.setInt(1, inventoryNode.get(QUANTITY_NAME).asInt());
          updateStatement.setString(2, inventoryNode.get(INGREDIENTS_NAME).asText());
          updateStatement.executeUpdate();
        }
      } else {
        // Ingredient does not exist, insert a new row
        try (PreparedStatement prepState =
            connection.prepareStatement(
                "INSERT INTO inventory (ing_name, quantity) VALUES (?, ?)")) {
          prepState.setString(1, inventoryNode.get(INGREDIENTS_NAME).asText());
          prepState.setInt(2, inventoryNode.get(QUANTITY_NAME).asInt());
          prepState.executeUpdate();
        }
      }
    }
  }

  /**
   * Gets the entire shopping list or inventory table from the database and populates the
   * corresponding Json file.
   *
   * @param listName The name of both the list and the table to get.
   * @throws DbConnectionErrorException If an error occurs while connecting to the database.
   */
  public static void getJsonFromDb(String listName) throws DbConnectionErrorException {
    try (Connection connection = DbLink.instance().getConnection();
        PreparedStatement prepState = connection.prepareStatement("SELECT * FROM " + listName);
        ResultSet resultSet = prepState.executeQuery()) {

      List<Ingredient> list = new ArrayList<>();

      while (resultSet.next()) {
        String ingName = resultSet.getString(INGREDIENTS_NAME);
        int quantity = resultSet.getInt(QUANTITY_NAME);

        Ingredient item = new Ingredient(ingName, quantity);

        list.add(item);
      }
      if (listName.equals(SHOPPING_LIST_STRING)) {
        listToJson(list, SHOPPING_LIST_STRING);
      }
      if (listName.equals(INVENTORY_STRING)) {
        listToJson(list, INVENTORY_STRING);
      }

    } catch (SQLException e) {
      LOGGER.severe("Error while getting JSON from DB: " + e.getMessage());
    }
  }

  /**
   * Updates either the shopping list or inventory table in the database with data from a JSON file.
   *
   * @param jsonFileName The name of the JSON file to update from, is also used to determine what
   *     table to insert into.
   */
  public static void updateFromJson(String jsonFileName) throws IllegalArgumentException {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      File jsonFile = new File(JSON_PATH + jsonFileName + JSON_EXTENSION);
      JsonNode rootNode = objectMapper.readTree(jsonFile);

      if (jsonFileName.equals(SHOPPING_LIST_STRING)) {
        for (JsonNode shoppingListNode : rootNode) {
          insertShoppingList(shoppingListNode);
        }
      } else if (jsonFileName.equals(INVENTORY_STRING)) {
        for (JsonNode inventoryNode : rootNode) {
          insertInventory(inventoryNode);
        }
      }

    } catch (IOException | SQLException | DbConnectionErrorException e) {
      throw new IllegalArgumentException("Error while updating from JSON: " + e.getMessage());
    }
  }

  /**
   * Inserts a shopping list item into the corresponding table while checking if ingredient already
   * exists .
   *
   * @param shoppingListNode The shopping list node to insert.
   * @throws SQLException If an error occurs while executing the SQL query.
   * @throws DbConnectionErrorException If an error occurs while connecting to the database.
   */
  private static void insertShoppingList(JsonNode shoppingListNode)
      throws SQLException, DbConnectionErrorException {
    try (Connection connection = DbLink.instance().getConnection()) {
      int count;
      // Check if the ingredient already exists in the database
      try (PreparedStatement checkIfExists =
          connection.prepareStatement("SELECT COUNT(*) FROM shopping_list WHERE ing_name = ?")) {
        checkIfExists.setString(1, shoppingListNode.get(INGREDIENTS_NAME).asText());
        ResultSet resultSet = checkIfExists.executeQuery();
        resultSet.next();
        count = resultSet.getInt(1);
        resultSet.close();
      }

      if (count > 0) {
        // Ingredient already exists, update the quantity
        try (PreparedStatement updateStatement =
            connection.prepareStatement(
                "UPDATE shopping_list SET quantity = ? WHERE ing_name = ?")) {
          updateStatement.setInt(1, shoppingListNode.get(QUANTITY_NAME).asInt());
          updateStatement.setString(2, shoppingListNode.get(INGREDIENTS_NAME).asText());
          updateStatement.executeUpdate();
        }
      } else {
        // Ingredient does not exist, insert a new row
        try (PreparedStatement prepState =
            connection.prepareStatement(
                "INSERT INTO shopping_list (ing_name, quantity) VALUES (?, ?)")) {
          prepState.setString(1, shoppingListNode.get(INGREDIENTS_NAME).asText());
          prepState.setInt(2, shoppingListNode.get(QUANTITY_NAME).asInt());
          prepState.executeUpdate();
        }
      }
    }
  }
}
