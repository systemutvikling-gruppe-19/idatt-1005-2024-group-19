package edu.ntnu.stud.controller.framecontroller.dialogboxes;

import java.util.Objects;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Abstract class for creating dialog boxes.
 * Contains methods for setting the style of the dialog box.
 */
public class DialogBox {

  // Objects for the dialog box
  private final Alert alert;
  private final Text headerText;
  private final Text contentText;
  private final int width;
  private final int height;

  /**
   * Constructor for the DialogBox class. Gets the variables needed to create the dialog box
   * from the subclasses.
   *
   * @param alert      The alert object to be displayed.
   * @param headerText The header text of the dialog box.
   * @param contentText The content text of the dialog box.
   * @param width      The width of the dialog box.
   * @param height     The height of the dialog box.
   */
  protected DialogBox(Alert alert, Text headerText, Text contentText, int width, int height) {
    this.alert = alert;
    this.headerText = headerText;
    this.contentText = contentText;
    this.width = width;
    this.height = height;
  }

  /**
   * Method to get the alert object.
   *
   * @return The alert object.
   */
  protected Alert getAlert() {
    return alert;
  }

  /**
   * Method to set the style of the dialog box.
   */
  protected void setStyle() {
    // Remove default content
    alert.setTitle(null);
    alert.setGraphic(null);
    alert.setHeaderText(null);

    // Set Icon for DialogBox
    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
    stage.getIcons().add(new Image(Objects.requireNonNull(
        getClass().getResourceAsStream("/logo.png"))));
    headerText.getStyleClass().add("titleText");

    // Set CSS for DialogBox
    alert.getDialogPane().getStylesheets().add(
        Objects.requireNonNull(getClass().getResource("/globals.css")).toExternalForm());
    alert.getDialogPane().getStyleClass().add("insideFrames");
    alert.getDialogPane().setPrefSize(width, height);

    // Set Text style for DialogBox
    headerText.getStyleClass().add("titleText");
    contentText.getStyleClass().add("breadText");
  }

  /**
   * Method to set the style of the positive button in the dialog box.
   * The button with the most importance.
   *
   * @param button The positive button.
   */
  protected void setPositiveButtonStyle(Button button) {
    button.getStyleClass().add("actionButton");
    button.setPrefSize(200, 40);
  }

  /**
   * Method to set the style of the negative button in the dialog box.
   * The button with the lesser importance.
   *
   * @param button The negative button.
   */
  protected void setNegativeButtonStyle(Button button) {
    button.getStyleClass().add("headerButton");
    button.setPrefSize(150, 40);
  }

  /**
   * Method to get the header text of the dialog box.
   *
   * @return The header text.
   */
  protected Text getHeaderText() {
    return headerText;
  }

  /**
   * Method to get the content text of the dialog box.
   *
   * @return The content text.
   */
  protected Text getContentText() {
    return contentText;
  }
}
