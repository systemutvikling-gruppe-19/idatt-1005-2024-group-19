package edu.ntnu.stud.controller.framecontroller.dialogboxes;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Dialog box to display an error message. The class extends the DialogBox class.
 *
 * @see DialogBox
 */
public class ErrorDialog extends DialogBox {

  /**
   * Constructor for the ErrorDialog class. Calls the constructor of the superclass.
   *
   * @param alert      The alert object to be displayed.
   * @param headerText The header text of the dialog box.
   * @param contentText The content text of the dialog box.
   * @param width      The width of the dialog box.
   * @param height     The height of the dialog box.
   */
  public ErrorDialog(Alert alert, Text headerText, Text contentText, int width, int height) {
    super(alert, headerText, contentText, width, height);
    this.setStyle();
    this.setPositiveButtonStyle((Button) alert.getDialogPane().lookupButton(ButtonType.OK));
  }

  /**
   * Method to show the dialog box for an error message.
   */
  public void showErrorDialog() {
    VBox vbox = new VBox();
    vbox.getChildren().addAll(getHeaderText(), getContentText());
    vbox.setSpacing(20);
    getAlert().getDialogPane().setContent(vbox);

    getAlert().showAndWait();
  }
}
