package edu.ntnu.stud.utils;

public class RecipeFrameException extends RuntimeException {
  public RecipeFrameException(String message, Throwable cause) {
    super(message, cause);
  }
}