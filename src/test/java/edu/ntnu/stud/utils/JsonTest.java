package edu.ntnu.stud.utils;

import org.json.JSONArray;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class JsonTest {

    @Test
    void testGetJsonArray() {
        try {
            JSONArray jsonArray = Json.getJsonArray("test");
            assertEquals(2, jsonArray.length());
            assertEquals("Sugar", jsonArray.getJSONObject(0).getString("ing_name"));
            assertEquals(5, jsonArray.getJSONObject(0).getInt("quantity"));
            assertEquals("Flour", jsonArray.getJSONObject(1).getString("ing_name"));
            assertEquals(3, jsonArray.getJSONObject(1).getInt("quantity"));
        } catch (FileNotFoundException e) {
            fail("File not found.");
        }
    }
}
