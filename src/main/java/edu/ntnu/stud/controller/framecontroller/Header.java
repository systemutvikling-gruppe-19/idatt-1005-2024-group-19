package edu.ntnu.stud.controller.framecontroller;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

/**
 * The Header class is a controller class for the header of the application. It contains buttons for
 * the cookbook, shopping list and inventory. The buttons are made up of a toggle button with an
 * icon and a label representing the number of notifications.
 */
public class Header {

  // Notification counts for each button
  private int cookBookNotificationCount;
  private int shoppingListNotificationCount;
  private int inventoryNotificationCount;

  // FXML elements
  @FXML
  @SuppressWarnings("unused")
  private StackPane cookBookPane;
  @FXML
  @SuppressWarnings("unused")
  private StackPane shoppingListPane;
  @FXML
  @SuppressWarnings("unused")
  private StackPane inventoryPane;
  @FXML
  private final Label cookBookNotification;
  @FXML
  private final Label shoppingListNotification;
  @FXML
  private final Label inventoryNotification;

  // Pane containing the header
  private final BorderPane headerPane;

  /**
   * Constructor for the Header class. Loads the FXML file and sets up the buttons with icons and
   * notification labels.
   *
   * @throws IOException If the FXML file cannot be loaded
   */
  public Header() throws IOException {
    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/header.fxml"));
    fxmlLoader.setController(this);
    this.headerPane = fxmlLoader.load();

    cookBookNotification = createNotificationLabel();
    shoppingListNotification = createNotificationLabel();
    inventoryNotification = createNotificationLabel();

    cookBookNotificationCount = 0;
    shoppingListNotificationCount = 0;
    inventoryNotificationCount = 0;

    setButtonGraphic(cookBookPane, "/icons/Book.png", cookBookNotification);
    setButtonGraphic(shoppingListPane, "/icons/Shopping Bag.png", shoppingListNotification);
    setButtonGraphic(inventoryPane, "/icons/Inventory.png", inventoryNotification);

    ToggleGroup toggleGroup = new ToggleGroup();
    ((ToggleButton) cookBookPane.getChildren().getFirst()).setToggleGroup(toggleGroup);
    ((ToggleButton) shoppingListPane.getChildren().getFirst()).setToggleGroup(toggleGroup);
    ((ToggleButton) inventoryPane.getChildren().getFirst()).setToggleGroup(toggleGroup);
  }

  /**
   * Returns the header pane.
   *
   * @return The header pane
   */
  public BorderPane getHeader() {
    return headerPane;
  }

  /**
   * Private method for creating a notification label for the header buttons.
   *
   * @return A label representing the number of notifications
   */
  private Label createNotificationLabel() {
    Label notification = new Label();
    notification.getStyleClass().add("notificationBubble");
    notification.setVisible(false);
    return notification;
  }

  /**
   * Private method for setting the graphic of a button with an icon and a notification label.
   *
   * @param buttonPane The pane containing the button
   * @param imagePath The path to the icon image
   * @param notificationLabel The notification label
   */
  private void setButtonGraphic(StackPane buttonPane, String imagePath, Label notificationLabel) {
    buttonPane.getChildren().add(notificationLabel);
    StackPane.setAlignment(notificationLabel, Pos.BOTTOM_RIGHT);

    HBox hbox = new HBox();
    hbox.getChildren().addAll(new ImageView(new Image(imagePath)));

    ToggleButton toggleButton = (ToggleButton) buttonPane.getChildren().getFirst();
    toggleButton.setGraphic(hbox);
  }

  /**
   * Increments the notification count for the cookbook button. This is unused in the current
   * implementation, because we do not have a use for notifications in the cookbook. The method is
   * kept for easy implementation of notifications in the future.
   */
  @SuppressWarnings("unused")
  public void incrementCookBookNotificationCount() {
    cookBookNotificationCount++;
    setNotificationCount(cookBookNotification, cookBookNotificationCount);
  }

  /**
   * Increments the notification count for the shopping list button.
   */
  public void incrementShoppingListNotificationCount() {
    shoppingListNotificationCount++;
    setNotificationCount(shoppingListNotification, shoppingListNotificationCount);
  }

  /**
   * Increments the notification count for the inventory button. This is unused in the current
   * implementation, because we do not have a use for notifications in the inventory. The method is
   * kept for easy implementation of notifications in the future.
   */
  @SuppressWarnings("unused")
  public void incrementInventoryNotificationCount() {
    inventoryNotificationCount++;
    setNotificationCount(inventoryNotification, inventoryNotificationCount);
  }

  /**
   * Clears the notification count for the cookbook button.
   */
  public void clearCookBookNotificationCount() {
    cookBookNotificationCount = 0;
    setNotificationCount(cookBookNotification, cookBookNotificationCount);
  }

  /**
   * Clears the notification count for the shopping list button.
   */
  public void clearShoppingListNotificationCount() {
    shoppingListNotificationCount = 0;
    setNotificationCount(shoppingListNotification, shoppingListNotificationCount);
  }

  /**
   * Clears the notification count for the inventory button.
   */
  public void clearInventoryNotificationCount() {
    inventoryNotificationCount = 0;
    setNotificationCount(inventoryNotification, inventoryNotificationCount);
  }

  /**
   * Private method for setting the notification count for a button. If the count is greater than 0,
   * the label turns visible. Otherwise, it is hidden.
   *
   * @param label The notification label
   * @param count The number of notifications
   */
  private void setNotificationCount(Label label, int count) {
    if (count > 0) {
      label.setText(String.valueOf(count));
      label.setVisible(true);
    } else {
      label.setVisible(false);
    }
  }

  /**
   * Returns the cookbook button.
   *
   * @return The cookbook button
   */
  public StackPane getCookBookButton() {
    return cookBookPane;
  }

  /**
   * Returns the shopping list button.
   *
   * @return The shopping list button
   */
  public StackPane getShoppingListButton() {
    return shoppingListPane;
  }

  /**
   * Returns the inventory button.
   *
   * @return The inventory button
   */
  public StackPane getInventoryButton() {
    return inventoryPane;
  }
}