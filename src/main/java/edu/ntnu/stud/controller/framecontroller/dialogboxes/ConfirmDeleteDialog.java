package edu.ntnu.stud.controller.framecontroller.dialogboxes;

import static edu.ntnu.stud.model.Inventory.clearInventory;
import static edu.ntnu.stud.model.ShoppingList.clearShoppingList;
import static edu.ntnu.stud.model.database.DbInit.clearTable;

import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Dialog box to display a choice for deleting ingredient lists. The class extends the DialogBox
 * class.
 *
 * @see DialogBox
 */
public class ConfirmDeleteDialog extends DialogBox {

  /**
   * Constructor for the ConfirmAddRecipeDialog class. Calls the constructor of the superclass.
   *
   * @param alert The alert object to be displayed.
   * @param headerText The header text of the dialog box.
   * @param contentText The content text of the dialog box.
   * @param width The width of the dialog box.
   * @param height The height of the dialog box.
   */
  public ConfirmDeleteDialog(
          Alert alert, Text headerText, Text contentText, int width, int height) {
    super(alert, headerText, contentText, width, height);
    this.setStyle();
  }

  /**
   * Method to show the dialog box for deleting ingredient list.
   *
   * @param tableName The name of the recipe to add to the shopping list.
   */
  public void showDeleteDialog(String tableName) {
    // create error label
    Label error = new Label();
    error.setTextFill(javafx.scene.paint.Color.RED);
    error.setText("No connection to remote storage. Function unavailable.");
    error.setVisible(false);

    // Set the style of the dialog box
    VBox vbox = new VBox();
    vbox.getChildren().addAll(getHeaderText(), getContentText(), error);
    vbox.setSpacing(20);
    getAlert().getDialogPane().setContent(vbox);

    // Create buttons for the dialog box
    ButtonType deleteLocal = new ButtonType("Locally");
    ButtonType deleteRemote = new ButtonType("Remotely");
    ButtonType deleteBoth = new ButtonType("Both");
    ButtonType cancel = new ButtonType("Done", ButtonBar.ButtonData.CANCEL_CLOSE);

    // Add buttons to the dialog box
    getAlert().getButtonTypes().setAll(deleteLocal, deleteRemote, deleteBoth, cancel);

    // Get the buttons from the dialog box as buttons
    Button deleteLocalButton = (Button) getAlert().getDialogPane().lookupButton(deleteLocal);
    Button deleteRemoteButton = (Button) getAlert().getDialogPane().lookupButton(deleteRemote);
    Button deleteBothButton = (Button) getAlert().getDialogPane().lookupButton(deleteBoth);
    Button cancelButton = (Button) getAlert().getDialogPane().lookupButton(cancel);

    // Set style for buttons
    setNegativeButtonStyle(deleteLocalButton);
    setNegativeButtonStyle(deleteRemoteButton);
    setNegativeButtonStyle(deleteBothButton);
    setPositiveButtonStyle(cancelButton);

    deleteRemoteButton.addEventFilter(
        ActionEvent.ACTION,
        event -> {
          try {
            clearTable(tableName);
          } catch (IllegalArgumentException e) {
            error.setVisible(true);
          }
          event.consume();
        });

    deleteBothButton.addEventFilter(
        ActionEvent.ACTION,
        event -> {
          try {
            clearTable(tableName);
              if (tableName.equals("inventory")) {
                  clearInventory();
              } else if (tableName.equals("shopping_list")) {
                  clearShoppingList();
              }
          } catch (IllegalArgumentException e) {
            error.setVisible(true);
          }
          event.consume();
        });

    deleteLocalButton.addEventFilter(
        ActionEvent.ACTION,
        event -> {
          if (tableName.equals("inventory")) {
            clearInventory();
          } else if (tableName.equals("shopping_list")) {
            clearShoppingList();
          }
          error.setVisible(false);
          event.consume();
        });

    // Show the dialog box and wait for the user to make a choice
    getAlert().showAndWait();
  }
}
