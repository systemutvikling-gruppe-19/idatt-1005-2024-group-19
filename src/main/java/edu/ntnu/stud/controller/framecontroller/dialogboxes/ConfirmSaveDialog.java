package edu.ntnu.stud.controller.framecontroller.dialogboxes;

import static edu.ntnu.stud.model.database.DbInit.clearTable;
import static edu.ntnu.stud.model.database.JsonToDb.updateFromJson;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Dialog box to display confirmation before saving the list to the database. to the shopping list.
 * The class extends the DialogBox class.
 *
 * @see DialogBox
 */
public class ConfirmSaveDialog extends DialogBox {
  /**
   * Constructor for the ConfirmSave class. Calls the constructor of the superclass.
   *
   * @param alert The alert object to be displayed.
   * @param headerText The header text of the dialog box.
   * @param contentText The content text of the dialog box.
   * @param width The width of the dialog box.
   * @param height The height of the dialog box.
   */
  public ConfirmSaveDialog(Alert alert, Text headerText, Text contentText, int width, int height) {
    super(alert, headerText, contentText, width, height);
    this.setStyle();
  }

  /**
   * Method to show the dialog confirmation before saving list to database.
   *
   * @param tableName The name of the recipe to add to the shopping list.
   */
  public void showSaveDialog(String tableName) {
    // create error label
    Label error = new Label();
    error.setTextFill(javafx.scene.paint.Color.RED);
    error.setText("No connection to remote storage. Function unavailable.");
    error.setVisible(false);

    VBox vbox = new VBox();
    vbox.getChildren().addAll(getHeaderText(), getContentText(), error);
    vbox.setSpacing(20);
    getAlert().getDialogPane().setContent(vbox);

    ButtonType yes = ButtonType.YES;
    ButtonType no = ButtonType.NO;

    getAlert().getButtonTypes().setAll(yes, no);

    // get the buttons
    Button yesButton = (Button) getAlert().getDialogPane().lookupButton(yes);
    Button noButton = (Button) getAlert().getDialogPane().lookupButton(no);

    setNegativeButtonStyle(yesButton);
    setPositiveButtonStyle(noButton);

    yesButton.addEventFilter(
        ActionEvent.ACTION,
        event -> {
          try {
            clearTable(tableName);
            updateFromJson(tableName);
          } catch (IllegalArgumentException e) {
            error.setVisible(true);
            event.consume();
          }
        });
    noButton.addEventFilter(
        ActionEvent.ACTION,
        event -> {
          getAlert().close();
          event.consume();
        });

    getAlert().showAndWait();
  }
}
