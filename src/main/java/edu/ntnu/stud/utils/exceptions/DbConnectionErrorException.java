package edu.ntnu.stud.utils.exceptions;

/**
 * Exception thrown when there is an error connecting to the database.
 */
public class DbConnectionErrorException extends Exception {

  /**
   * Constructs a new DbConnectionErrorException with the specified detail message and cause.
   *
   * @param message the detail message (which is saved for later retrieval
   *                by the getMessage() method).
   * @param cause the cause (which is saved for later retrieval by the getCause() method).
   */
  public DbConnectionErrorException(String message, Throwable cause) {
    super(message, cause);
  }
}
