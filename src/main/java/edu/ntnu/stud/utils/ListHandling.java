package edu.ntnu.stud.utils;

import static edu.ntnu.stud.utils.InputValid.validateInt;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import edu.ntnu.stud.model.Ingredient;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Utility class for handling lists.
 */
public class ListHandling {

  // Logger
  private static final Logger LOGGER = LoggerUtil.getLogger(ListHandling.class);

  /**
   * Add or subtract ingredients from a list. The integer at the end of the method name
   * indicates the operation to perform. 1 for addition, -1 for subtraction.
   *
   * @param mainList The main list to add or subtract ingredients from.
   * @param subList The list of ingredients to add or subtract.
   * @param operation The operation to perform. 1 for addition, -1 for subtraction.
   * @return The updated main list.
   */
  public static List<Ingredient> addSubtractFromList(List<Ingredient> mainList,
                                                     List<Ingredient> subList, int operation) {
    for (Ingredient subListItem : subList) {
      String subListItemString = subListItem.getIngName();

      // Check if ingredient already exists in the shopping list
      boolean ingredientExists = checkIfIngredientInList(mainList, subListItemString);

      if (ingredientExists) {
        // If the ingredient exists, update its quantity
        for (Ingredient mainListItem : mainList) {
          if (mainListItem.getIngName().equalsIgnoreCase(subListItemString)) {
            int newQuantity = mainListItem.getQuantity() + (operation * subListItem.getQuantity());
            mainListItem.setQuantity(newQuantity);
            break; // No need to iterate further
          }
        }
      } else {
        // If the ingredient does not exist, add it to the shopping list
        mainList.add(subListItem);
      }
    }
    return mainList;
  }

  /**
   * Check if an ingredient is in a list.
   *
   * @param list The list to check.
   * @param ingredientName The name of the ingredient to check for.
   * @return True if the ingredient is in the list, false otherwise.
   */
  private static boolean checkIfIngredientInList(List<Ingredient> list, String ingredientName) {
    for (Ingredient ingredient : list) {
      if (ingredient.getIngName().equalsIgnoreCase(ingredientName)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Add an ingredient to a list.
   *
   * @param inputList The list to add the ingredient to.
   * @param ingName The name of the ingredient to add.
   * @param quantity The quantity of the ingredient to add.
   * @throws IllegalArgumentException If the quantity is not an integer.
   */
  public static void addIngredientToList(List<Ingredient> inputList, String ingName,
                                         String quantity) throws IllegalArgumentException {
    boolean ingredientExists = false;
    validateInt(quantity);

    // Check if the ingredient already exists in the list, if so, update the quantity
    for (Ingredient ingredient : inputList) {
      if (ingredient.getIngName().trim().equalsIgnoreCase(ingName.trim())) {
        ingredient.setQuantity(ingredient.getQuantity() + Integer.parseInt(quantity));
        ingredientExists = true;
        break;
      }
    }

    // If the ingredient doesn't exist, add it to the updatedList
    if (!ingredientExists) {
      // Create a new ingredient node and add it to the updatedList
      inputList.add(new Ingredient(ingName, Integer.parseInt(quantity)));
    }
  }

  /**
   * Get the ingredients from a recipe.
   *
   * @param recName The name of the recipe to get the ingredients from.
   * @return A list of ingredients from the recipe.
   */
  public static List<Ingredient> getIngredientsFromRecipe(String recName) {
    ObjectMapper objectMapper = new ObjectMapper();
    List<Ingredient> recipeIngredients = new ArrayList<>();

    try {
      File cookbookJson = new File("src/main/resources/json/cookbook.json");
      ArrayNode cookbookNode = (ArrayNode) objectMapper.readTree(cookbookJson);

      // Iterate over the cookbook to find the recipe with the provided name
      for (JsonNode recipeNode : cookbookNode) {
        String recipeName = recipeNode.get("rec_name").asText();
        if (recipeName.equalsIgnoreCase(recName)) {
          // Found the recipe, get its ingredients
          ArrayNode recipeIngredientsNode = (ArrayNode) recipeNode.get("ingredients");

          for (JsonNode ingredientNode : recipeIngredientsNode) {
            String ingName = ingredientNode.get("ing_name").asText();
            int quantity = ingredientNode.get("quantity").asInt();
            recipeIngredients.add(new Ingredient(ingName, quantity));

          }
          break; // Exit loop once the recipe is found
        }
      }

    } catch (IOException e) {
      LOGGER.severe("Error reading cookbook.json: " + e.getMessage());
    }
    return recipeIngredients;
  }
}
