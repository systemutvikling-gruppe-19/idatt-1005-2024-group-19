package edu.ntnu.stud.controller.framecontroller;

import edu.ntnu.stud.controller.MainApplication;
import edu.ntnu.stud.controller.framecontroller.dialogboxes.ConfirmAddRecipeDialog;
import edu.ntnu.stud.utils.LoggerUtil;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Frame used to display the cook book. This is the controller class for the
 * cookBookFrame.fxml file. The frame displays the recipes from a JSON file
 * in a scrollable view. Each recipe is displayed with an image and a name.
 * The user can click on the "More" button to view more information about
 * a specific recipe.
 */
public class CookBookFrame {

  // Logger
  private static final Logger LOGGER = LoggerUtil.getLogger(CookBookFrame.class);

  // Constants
  private static final String COOKBOOK_FRAME_FXML = "/fxml/cookBookFrame.fxml";
  private static final String STYLING_WHITE = "-fx-background-color: white";
  private static final String STYLING_BLACK = "-fx-background-color: black";
  private static final String FONT_STYLE = "-fx-font: 48 arial";
  private static final String IMAGE_EFFECT =
      "-fx-effect: innershadow(gaussian, rgba(0,0,0,0.3), 10, 0.0, 0, 10)";

  private static final int ARC_WIDTH = 35;
  private static final int ARC_HEIGHT = 35;
  private static final int IMAGE_WIDTH = 500;
  private static final int IMAGE_HEIGHT = 500;

  // Class Fields
  @FXML
  private final ScrollPane frame;
  private final CookBookJsonReader jsonReader;
  private final CookBookFrameComponents frameComponents;
  private final CookBookButtonHandler buttonHandler;

  /**
   * The constructor for this class creates a new CookBookFrame object.
   * The constructor initializes the JSON reader, frame components and
   * button handler, and loads the cookBookFrame.fxml file.
   * These components are used to divide the responsibilities of the
   * class and to create and manage the frame.
   *
   * @see CookBookJsonReader
   * @see CookBookFrameComponents
   * @see CookBookButtonHandler
   *
   * @param mainApplication The main application object, used for the buttons to
   *            interact with the main application.
   * @throws IOException If the fxml file is not found.
   */
  public CookBookFrame(MainApplication mainApplication) throws IOException {
    this.jsonReader = new CookBookJsonReader();
    this.frameComponents = new CookBookFrameComponents();
    this.buttonHandler = new CookBookButtonHandler(mainApplication);

    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(COOKBOOK_FRAME_FXML));
    fxmlLoader.setController(this);
    this.frame = fxmlLoader.load();
  }

  /**
   * Sets the image of a recipe with the given filepath. The method reads
   * the JSON file to get this filepath. If the file is not found,
   * a FileNotFoundException is thrown. The message of the exception is then
   * logged.
   *
   * @see CookBookJsonReader
   * @see CookBookFrameComponents
   *
   * @param filepath The path to the JSON file containing the recipes.
   */
  public void setImageFromJson(String filepath) {
    try {
      JSONArray jsonArray = jsonReader.getJsonArray(filepath);
      VBox cookBookVbox = frameComponents.createCookBookVbox(jsonArray);
      HBox hboxWrapper = new HBox(cookBookVbox);
      frame.setContent(hboxWrapper);
    } catch (FileNotFoundException e) {
      LOGGER.severe("File not found: " + e.getMessage());
    }
  }

  /**
   * Nested method to read the image of a recipe from the JSON file.
   *
   * @see CookBookJsonReader
   *
   * @param i The index of the recipe in the JSON file.
   * @param jsonArray The JSON array containing the recipes.
   * @return The image of the recipe at the given index.
   */
  public String readImageFromJson(int i, JSONArray jsonArray) {
    return jsonReader.readImageFromJson(i, jsonArray);
  }

  /**
   * Nested method to read the name of a recipe from the JSON file.
   *
   * @see CookBookJsonReader
   *
   * @param i The index of the recipe in the JSON file.
   * @param jsonArray The JSON array containing the recipes.
   * @return The name of the recipe at the given index.
   */
  public String readNameOfRecipe(int i, JSONArray jsonArray) {
    return jsonReader.readNameOfRecipe(i, jsonArray);
  }

  /**
   * Nested method to set the "Add" and "More" buttons for a recipe.
   *
   * @see CookBookButtonHandler
   *
   * @param index The index of the recipe in the JSON file.
   * @param jsonArray The JSON array containing the recipes.
   * @param rowCounter The row number of the recipe in the frame.
   * @return The HBox containing the "Add" and "More" buttons for the recipe.
   */
  public HBox setAddMoreButtons(int index, JSONArray jsonArray, int rowCounter) {
    return buttonHandler.setAddMoreButtons(index, jsonArray, rowCounter, jsonReader);
  }

  /**
   * Returns the CookBook frame.
   *
   * @return The CookBook frame.
   */
  public ScrollPane getCookBookFrame() {
    return frame;
  }

  /**
   * Nested class to read the JSON file containing the recipes. The class
   * reads the JSON file and returns the JSON array containing the recipes.
   * The class also reads the image and name of a recipe at a given index.
   */
  protected static class CookBookJsonReader {

    /**
     * Reads the JSON file at the given filepath and returns the JSON array
     * containing the recipes. If the file is not found, a FileNotFoundException
     * is thrown.
     *
     * @param filepath The path to the JSON file containing the recipes.
     * @return The JSON array containing the recipes.
     * @throws FileNotFoundException If the file is not found.
     */
    public JSONArray getJsonArray(String filepath) throws FileNotFoundException {
      FileReader fileReader = new FileReader(filepath);
      JSONTokener jsonTokener = new JSONTokener(fileReader);
      return new JSONArray(jsonTokener);
    }

    /**
     * Reads the image of a recipe from the JSON array at the given index.
     *
     * @param i The index of the recipe in the JSON array.
     * @param jsonArray The JSON array containing the recipes.
     * @return The image of the recipe at the given index.
     */
    public String readImageFromJson(int i, JSONArray jsonArray) {
      JSONObject jsonObject = jsonArray.getJSONObject(i);
      return jsonObject.getString("im_path");
    }

    /**
     * Reads the name of a recipe from the JSON array at the given index.
     *
     * @param i The index of the recipe in the JSON array.
     * @param jsonArray The JSON array containing the recipes.
     * @return The name of the recipe at the given index.
     */
    public String readNameOfRecipe(int i, JSONArray jsonArray) {
      JSONObject jsonObject = jsonArray.getJSONObject(i);
      return jsonObject.getString("rec_name");
    }
  }

  /**
   * Nested class to create the components of the CookBook frame. The class
   * creates the VBox, HBox, BorderPane, ImageView, Label and buttons for
   * each recipe in the JSON array. The class also sets the style of the
   * components.
   */
  protected class CookBookFrameComponents {

    /**
     * Creates the main VBox for the CookBook frame. The method loops through
     * each recipe in the JSON array and creates a BorderPane for each recipe.
     *
     * @param jsonArray The JSON array containing the recipes.
     * @return A VBox containing the recipes in the JSON array.
     */
    private VBox createCookBookVbox(JSONArray jsonArray) {
      int columnCounter = 0;
      int rowCounter = 1;
      HBox currentHbox = new HBox();
      VBox cookBookVbox = new VBox();

      HBox.setHgrow(cookBookVbox, Priority.ALWAYS);
      HBox.setHgrow(currentHbox, Priority.ALWAYS);

      for (int i = 0; i < jsonArray.length(); i++) {
        BorderPane borderPane = createBorderPane(i, jsonArray, rowCounter, currentHbox);
        currentHbox.getChildren().add(borderPane);
        columnCounter++;

        // Check whether a new row is starting
        if ((columnCounter % 2 == 0) || i == jsonArray.length() - 1) {
          columnCounter = 0;
          rowCounter++;
          currentHbox = createNewHbox(cookBookVbox, currentHbox);
        }
      }
      return cookBookVbox;
    }

    /**
     * Creates a BorderPane for a recipe in the JSON array. The method creates
     * a VBox for the recipe name and buttons, and an ImageView for the recipe
     * image. The VBox and ImageView are added to the BorderPane.
     *
     * @param i The index of the recipe in the JSON array.
     * @param jsonArray The JSON array containing the recipes.
     * @param rowCounter The row number of the recipe in the frame.
     * @param currentHbox The current HBox containing the recipes.
     * @return A BorderPane containing the recipe image and name.
     */
    private BorderPane createBorderPane(int i, JSONArray jsonArray,
                                        int rowCounter, HBox currentHbox) {
      BorderPane borderPane = new BorderPane();
      VBox newVbox = createVbox(i, jsonArray, rowCounter, currentHbox);
      ImageView newImageView = createImageView(i, jsonArray);

      borderPane.setCenter(newImageView);
      borderPane.setTop(newVbox);
      return borderPane;
    }

    /**
     * Creates a VBox for a recipe in the JSON array. The method creates a
     * Label for the recipe name and buttons, and adds the buttons to the VBox.
     *
     * @param i The index of the recipe in the JSON array.
     * @param jsonArray The JSON array containing the recipes.
     * @param rowCounter The row number of the recipe in the frame.
     * @param currentHbox The current HBox containing the recipes.
     * @return A VBox containing the recipe name and buttons.
     */
    private VBox createVbox(int i, JSONArray jsonArray, int rowCounter, HBox currentHbox) {
      Label text = new Label(readNameOfRecipe(i, jsonArray));
      setLabelStyle(text, rowCounter, currentHbox);
      VBox newVbox = new VBox(text, setAddMoreButtons(i, jsonArray, rowCounter));
      newVbox.setPadding(new Insets(10, 0, 5, 0));
      newVbox.setAlignment(Pos.CENTER);
      return newVbox;
    }

    /**
     * Sets the style of the Label for the recipe name.
     *
     * @param text The Label for the recipe name.
     * @param rowCounter The row number of the recipe in the frame.
     * @param currentHbox The current HBox containing the recipes.
     */
    private void setLabelStyle(Label text, int rowCounter, HBox currentHbox) {
      if (rowCounter % 2 == 0) {
        text.setTextFill(Color.WHITE);
      } else {
        currentHbox.setStyle(STYLING_WHITE);
      }
      text.setStyle(FONT_STYLE);
    }

    /**
     * Creates an ImageView for the recipe image. The method sets the image
     * of the ImageView and adds a clip and effect to the ImageView.
     *
     * @param i The index of the recipe in the JSON array.
     * @param jsonArray The JSON array containing the recipes.
     * @return An ImageView containing the recipe image.
     */
    private ImageView createImageView(int i, JSONArray jsonArray) {
      ImageView newImageView = new ImageView();
      setImage(newImageView, readImageFromJson(i, jsonArray));
      Rectangle clip = new Rectangle(newImageView.getFitWidth(), newImageView.getFitHeight());
      clip.setArcWidth(ARC_WIDTH);
      clip.setArcHeight(ARC_HEIGHT);
      newImageView.setClip(clip);
      newImageView.setStyle(IMAGE_EFFECT);
      return newImageView;
    }

    /**
     * Creates a new HBox for the recipes in the JSON array. The method sets
     * the spacing, alignment, padding and style of the HBox, and adds the
     * HBox to the VBox containing the recipes.
     *
     * @param cookBookVbox The VBox containing the recipes.
     * @param currentHbox The current HBox containing the recipes.
     * @return A new HBox for the recipes in the JSON array.
     */
    private HBox createNewHbox(VBox cookBookVbox, HBox currentHbox) {
      currentHbox.setSpacing(81);
      currentHbox.setAlignment(Pos.CENTER);
      currentHbox.setPadding(new Insets(0, 0, 30, 0));
      cookBookVbox.getChildren().add(currentHbox);
      currentHbox = new HBox();
      currentHbox.setStyle(STYLING_BLACK);
      return currentHbox;
    }

    /**
     * Sets the image of an ImageView with the given image path.
     *
     * @param imageView The ImageView to set the image to.
     * @param image The path to the image to set.
     */
    private void setImage(ImageView imageView, String image) {
      Image loadedImage = new Image(image);

      // Set the width and height of the ImageView
      imageView.setFitWidth(CookBookFrame.IMAGE_WIDTH);
      imageView.setFitHeight(CookBookFrame.IMAGE_HEIGHT);

      // Set the image to the ImageView
      imageView.setImage(loadedImage);
    }
  }

  /**
   * Nested class to handle the "Add" and "More" buttons for the recipes. The
   * class creates the buttons and sets the actions for the buttons. The class
   * also sets the style of the buttons.
   */
  protected static class CookBookButtonHandler {

    // The main application object
    private final MainApplication mainApplication;

    /**
     * The constructor for this class creates a new CookBookButtonHandler object.
     * This is used to set the main application object. for the buttons in this
     * class to interact with.
     *
     * @param mainApplication The main application object.
     */
    public CookBookButtonHandler(MainApplication mainApplication) {
      this.mainApplication = mainApplication;
    }

    /**
     * Sets the "Add" and "More" buttons for a recipe in the JSON array. The
     * method creates the buttons and sets the actions for the buttons. The
     * method also sets the style of the buttons.
     *
     * @param index The index of the recipe in the JSON array.
     * @param jsonArray The JSON array containing the recipes.
     * @param rowCounter The row number of the recipe in the frame.
     * @param jsonHandler The JSON reader object.
     * @return The HBox containing the "Add" and "More" buttons for the recipe.
     */
    public HBox setAddMoreButtons(int index, JSONArray jsonArray,
                                  int rowCounter, CookBookJsonReader jsonHandler) {
      Button add = new Button("Add  >");
      Button more = new Button("More  >");
      add.getStyleClass().add("addAndMore");
      more.getStyleClass().add("addAndMore");

      if (rowCounter % 2 == 0) {
        add.getStyleClass().add("white");
        more.getStyleClass().add("white");
      } else {
        add.getStyleClass().add("black");
        more.getStyleClass().add("black");
      }
      more.setOnAction(e -> mainApplication.showRecipeFrame(index, jsonArray));

      add.setOnAction(e -> {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        Text headerText = new Text("Add recipe to Shopping List");
        Text contentText = new Text("Do you want to add this entire recipe to the Shopping List \n"
            + "or just the ingredients you dont have in the Inventory?");

        //Create confirmAddRecipeDialog
        ConfirmAddRecipeDialog confirmAddRecipeDialog = new ConfirmAddRecipeDialog(
            alert, headerText, contentText, 700, 300, mainApplication.getHeader());
        confirmAddRecipeDialog.showAddRecipeChoiceDialog(
            jsonHandler.readNameOfRecipe(index, jsonArray));
      });

      HBox newHbox = new HBox(add, more);
      newHbox.setAlignment(Pos.CENTER);
      newHbox.setSpacing(60);
      return newHbox;
    }
  }
}
