package edu.ntnu.stud.utils.exceptions;

/**
 * Exception thrown when an item list cannot be updated.
 */
public class UpdateItemException extends RuntimeException {
  /**
   * Constructs a new UpdateItemException with the specified detail message.
   *
   * @param message the detail message (which is saved for later retrieval
   *                by the getMessage() method).
   * @param cause the cause (which is saved for later retrieval by the getCause() method).
   */
  public UpdateItemException(String message, Throwable cause) {
    super(message, cause);
  }
}
