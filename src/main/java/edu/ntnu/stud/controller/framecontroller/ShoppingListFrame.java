package edu.ntnu.stud.controller.framecontroller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

/**
 * A frame for displaying the shopping list. It extends the ItemListFrame and adds any specific
 * functionality for the shopping list.
 *
 * @see ItemListFrame
 */
public class ShoppingListFrame extends ItemListFrame implements JsonReadable {

  // The buttons in the shopping list frame
  @FXML
  @SuppressWarnings("unused")
  private Button setShoppingListToDbButton;
  @FXML
  @SuppressWarnings("unused")
  private Button shoppingClearButton;
  @FXML
  @SuppressWarnings("unused")
  private Button addIngredientButton;
  @FXML
  @SuppressWarnings("unused")
  private Button shoppingListGetButton;

  /**
   * Uses the constructor of the superclass to set the FXML file for the shopping list frame.
   */
  public ShoppingListFrame() {
    super("/fxml/shoppingListFrame.fxml");
  }

  /**
   * Sets the event handler for the set shopping list to database button.
   *
   * @param event The event handler for the button.
   */
  public void setShoppingListToDbButtonAction(EventHandler<ActionEvent> event) {
    setShoppingListToDbButton.setOnAction(event);
  }

  /**
   * Sets the event handler for the clear shopping list button.
   *
   * @param event The event handler for the button.
   */
  public void setShoppingClearButtonAction(EventHandler<ActionEvent> event) {
    shoppingClearButton.setOnAction(event);
  }

  /**
   * Sets the event handler for the add ingredient button.
   *
   * @param event The event handler for the button.
   */
  public void setAddIngredientButtonAction(EventHandler<ActionEvent> event) {
    addIngredientButton.setOnAction(event);
  }

  /**
   * Sets the event handler for the get shopping list button.
   *
   * @param event The event handler for the button.
   */
  public void setShoppingListGetButtonAction(EventHandler<ActionEvent> event) {
    shoppingListGetButton.setOnAction(event);
  }
}