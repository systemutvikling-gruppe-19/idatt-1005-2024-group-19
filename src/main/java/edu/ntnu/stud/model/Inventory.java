package edu.ntnu.stud.model;

import static edu.ntnu.stud.utils.Json.jsonToIngredientList;
import static edu.ntnu.stud.utils.Json.listToJson;

import edu.ntnu.stud.utils.ListHandling;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javafx.util.Pair;

/**
 * The Inventory class is responsible for managing the inventory of ingredients.
 */
public class Inventory {

  // List of ingredients in the inventory
  private static List<Ingredient> inventory = new ArrayList<>();

  // Constants
  private static final String INVENTORY_STRING = "inventory";

  /**
   * Adds a recipe to the inventory by reading the recipe from cookbook.json
   * and adding it to inventory.json.
   *
   * @param recipeName The name of the recipe to add to the inventory
   */
  public static void reduceInventoryByRecipe(String recipeName) {
    List<Ingredient> newInventory;
    newInventory = ListHandling.addSubtractFromList(getInventory(),
        ListHandling.getIngredientsFromRecipe(recipeName), -1);

    for (Ingredient ingredient : newInventory) {
      if (ingredient.getQuantity() < 0) {
        ingredient.setQuantity(0);

      }
    }
    listToJson(newInventory, INVENTORY_STRING);
  }

  /**
   * Get the inventory list from inventory.json.
   *
   * @return The inventory list
   */
  public static List<Ingredient> getInventory() {
    inventory.clear();
    jsonToIngredientList(inventory, INVENTORY_STRING);
    return inventory;
  }

  /**
   * Get the inventory list from inventory.json without ingredients with quantity 0.
   *
   * @return The inventory list without ingredients with quantity 0
   */
  public static Collection<Pair<String, String>> showInventoryWithoutZero(
      List<Ingredient> inventory) {
    Collection<Pair<String, String>> displayInventory = new ArrayList<>();
    for (Ingredient ingredient : inventory) {
      if (ingredient.getQuantity() > 0) {
        displayInventory.add(new Pair<>(ingredient.getIngName(),
            String.valueOf(ingredient.getQuantity())));
      }
    }
    return displayInventory;
  }

  /**
   * Clear the inventory by removing all ingredients from inventory.json.
   */
  public static void clearInventory() {
    inventory.clear();
    listToJson(inventory, INVENTORY_STRING);
  }
}



