package edu.ntnu.stud.utils;

/**
 * Utility class for validating input.
 */
public class InputValid {

  /**
   * Validates that the input is a positive number.
   *
   * @param input the input to validate.
   * @throws IllegalArgumentException if the input is not a positive number.
   */
  public static void validateInt(String input) {
    try {
      if (Integer.parseInt(input) <= 0) {
        throw new IllegalArgumentException("Input must be a positive number.");
      }
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Input must be an number.");
    }
  }
}
