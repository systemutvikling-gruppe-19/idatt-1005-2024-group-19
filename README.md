# QUICK LIST

## Project description
The following is a direct copy of the assignment description given by Department of Computer Science at NTNU:

“The team's task is to develop a tool for private households, student collectives, health institutions or similar places for planning of purchases. The application must provide an overview of food inventory, have a cookbook with recipes and generate a shopping list based on the selected recipe in the cookbook as well as the current inventory.”

## Installation guide 
To install and run the code simply clone the project to your IDE of choice or download a zipped version of the source code. To actually run the code it is necessary for the machine to already have a Java Development Kit, at least version 1.7 or above, and Maven downloaded. More information on how to do this can be found on this projects wiki page.

Once completed the user can open the terminal and run the commands "mvn javafx:run".

For the user to access all functionality it is also required that the user runs an SQL server locally on their machine. Eg. MySQL, with the username "root" and empty password. If you wish to have a different username and or password, go to "src/main/java/edu/ntnu/stud/model/database/DbLink.java" and change the values this.username and this.password in the constructor to the desired username and password.

## Using the program
Once running the program you will be greated to the main page in the application titled "Cookbok". Here you can scroll through the different recipes and click on the one you find the most appetizing. On every page there is a header which lets you navigate freely between all the different pages.

Clicking on the inventory button will take you to your inventory. Here you are able to view all the items in your inventory and the quantity, add new items to your inventory, and clear the inventory.

Clicking on the shopping list button takes you to the shopping list page. Here you have the option to add item to your shopping cart, save your shopping cart, and delete the shopping cart.

Each recipe on the Cookbook page has the option has the buttons "Add" and "More". Clicking on "Add" will give you a promp to either add all the ingredients from that recipe to the shopping cart, or only the ingredients that are missing from your inventory. Pressing the "More" button takes you to another page displaying a picture of the recipe, necessary ingredients, and also the steps to making the food. Here you will also have an "Add" button which gives you the same prompts as explained earlier.

To close the application simply press the close symbol in the top right corner.
