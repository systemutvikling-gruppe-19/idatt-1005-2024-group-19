package edu.ntnu.stud.model.database;

import edu.ntnu.stud.utils.exceptions.DbConnectionErrorException;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static edu.ntnu.stud.model.database.DbInit.*;

class DbInitTest {

    @Test
    void initializationTest() {
        DbInit.initializeDatabase();
    }

    @Test
    void test_init_Preexisting() {
        DbInit.initializeDatabase();
        DbInit.initializeDatabase();
    }

    // USE THIS TO DELETE A DATABASE
    @Test
    void test_dropDatabase() throws SQLException, DbConnectionErrorException {
        Connection connection = DbLink.instance().getConnection();
        Statement statement = connection.createStatement();

        dropDatabase(statement);
    }

    @Test
    void test_init_NoDatabase() throws SQLException, DbConnectionErrorException {
        DbInit.initializeDatabase();

        Connection connection = DbLink.instance().getConnection();
        Statement statement = connection.createStatement();

        dropDatabase(statement);

        DbInit.initializeDatabase();
    }
}