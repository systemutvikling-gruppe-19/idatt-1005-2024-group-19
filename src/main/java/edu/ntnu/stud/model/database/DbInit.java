package edu.ntnu.stud.model.database;

import edu.ntnu.stud.utils.LoggerUtil;
import edu.ntnu.stud.utils.exceptions.DbConnectionErrorException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 * Initializes the database and creates the necessary tables if they do not exist.
 */
public class DbInit {

  private static final Logger LOGGER = LoggerUtil.getLogger(DbInit.class);

  /**
   * Initializes the database and creates the necessary tables if they do not exist.
   *
   * @throws IllegalArgumentException If the database initialization fails.
   */
  public static void initializeDatabase() throws IllegalArgumentException {
    try (Connection connection = DbLink.instance().getConnectionNoDB("");
         Statement statement = connection.createStatement()) {

      DatabaseMetaData dbm = connection.getMetaData();
      ResultSet catalog = dbm.getCatalogs();
      boolean dbExists = false;

      // Iterate over the result set and check if the "quicklist" database exists
      while (catalog.next()) {
        String catalogName = catalog.getString("TABLE_CAT");
        if (catalogName.equals("quicklist")) {
          dbExists = true;
          break; // Exit loop early since we found the database
        }
      }

      // If the database exists, use it. If not, create it and use it.
      if (dbExists) {
        useDatabase(statement);
      } else {
        createDatabase(statement);
        useDatabase(statement);
        createTables(statement);
      }
    } catch (SQLException e) {
      throw new IllegalArgumentException("Database initialization failed");
    } catch (DbConnectionErrorException e) {
      throw new IllegalArgumentException("Database connection failed");
    }
  }

  /**
   * Creates the necessary tables in the database if they do not exist.
   *
   * @param statement The statement to execute the SQL query.
   * @throws SQLException If an error occurs while executing the SQL query.
   */
  private static void createTables(Statement statement) throws SQLException {
    statement.executeUpdate("CREATE TABLE IF NOT EXISTS cookbook ("
        + "rec_name VARCHAR(30) PRIMARY KEY,"
        + "im_path VARCHAR(200),"
        + "instruction TEXT"
        + ") ENGINE = InnoDB;");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS recipe_ingredients ("
        + "rec_name VARCHAR(30),"
        + "ing_name VARCHAR(30),"
        + "quantity INT,"
        + "PRIMARY KEY(rec_name, ing_name),"
        + "FOREIGN KEY (rec_name) REFERENCES cookbook(rec_name)"
        + ") ENGINE = InnoDB;");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS inventory ("
        + "ing_name VARCHAR(30) PRIMARY KEY,"
        + "quantity INT"
        + ") ENGINE = InnoDB;");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS shopping_list ("
        + "ing_name VARCHAR(30) PRIMARY KEY,"
        + "quantity INT"
        + ") ENGINE = InnoDB;");
  }

  /**
   * Creates the "quicklist" database if it does not exist.
   *
   * @param statement The statement to execute the SQL query.
   * @throws SQLException If an error occurs while executing the SQL query.
   */
  private static void createDatabase(Statement statement) throws SQLException {
    statement.executeUpdate("CREATE DATABASE IF NOT EXISTS quicklist;");
  }

  /**
   * Drops the "quicklist" database if it exists.
   *
   * @param statement The statement to execute the SQL query.
   * @throws SQLException If an error occurs while executing the SQL query.
   */
  public static void dropDatabase(Statement statement) throws SQLException {
    statement.executeUpdate("DROP DATABASE IF EXISTS quicklist;");
  }

  /**
   * Uses the "quicklist" database.
   *
   * @param statement The statement to execute the SQL query.
   * @throws SQLException If an error occurs while executing the SQL query.
   */
  private static void useDatabase(Statement statement) throws SQLException {
    statement.executeUpdate("USE quicklist;");
  }

  /**
   * Clears the specified table.
   *
   * @param tableName The name of the table to clear.
   */
  public static void clearTable(String tableName) throws IllegalArgumentException {
    try (Connection connection = DbLink.instance().getConnection();
         Statement statement = connection.createStatement()) {
      statement.executeUpdate("DELETE FROM " + tableName + ";");
    } catch (SQLException | DbConnectionErrorException e) {
      LOGGER.severe("Failed to clear table: " + tableName);
      throw new IllegalArgumentException("Failed to clear table");
    }
  }
}