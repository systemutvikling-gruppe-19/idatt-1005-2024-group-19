package edu.ntnu.stud.model.database;

import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import edu.ntnu.stud.utils.exceptions.DbConnectionErrorException;
import org.junit.After;
import org.junit.Test;

public class DbLinkTest {

    private Connection connection;
    private PreparedStatement preparedStatement;     // ?
    private ResultSet resultSet;                     // ?

    @After
    public void tearDown() throws Exception {
        DbLink.close(connection, preparedStatement, resultSet);
    }

    @Test
    public void test_DbLink_withDb() {
        DbInit.initializeDatabase();
        DbLink.close(connection, preparedStatement, resultSet);

        // Initialize the database connection
        DbLink dbLink = DbLink.instance();
      try {
        connection = dbLink.getConnection();
      } catch (DbConnectionErrorException e) {
        throw new RuntimeException(e);
      }

      // Test that the connection is not null
        assertNotNull("Connection should not be null", connection);
    }

    @Test
    public void test_DbLink_NoDb() {
        // Initialize the database connection
        DbLink dbLink = DbLink.instance();
      try {
        connection = dbLink.getConnectionNoDB("");
      } catch (DbConnectionErrorException e) {
        throw new RuntimeException(e);
      }

      // Test that the connection is not null
        assertNotNull("Connection should not be null", connection);
    }
}