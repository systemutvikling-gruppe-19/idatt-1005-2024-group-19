package edu.ntnu.stud.controller.framecontroller.dialogboxes;

import static edu.ntnu.stud.model.ShoppingList.getShoppingList;
import static edu.ntnu.stud.model.ShoppingList.updateShoppingList;
import static edu.ntnu.stud.utils.Json.listToJson;
import static edu.ntnu.stud.utils.ListHandling.getIngredientsFromRecipe;

import edu.ntnu.stud.controller.framecontroller.Header;
import edu.ntnu.stud.utils.ListHandling;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Dialog box to display a choice between adding an entire recipe or only the remaining ingredients
 * to the shopping list. The class extends the DialogBox class.
 *
 * @see DialogBox
 */
public class ConfirmAddRecipeDialog extends DialogBox {

  /**
   * The header of the application.
   */
  private final Header guiHeader;

  /**
   * Constructor for the ConfirmAddRecipeDialog class. Calls the constructor of the superclass.
   *
   * @param alert      The alert object to be displayed.
   * @param headerText The header text of the dialog box.
   * @param contentText The content text of the dialog box.
   * @param width      The width of the dialog box.
   * @param height     The height of the dialog box.
   * @param guiHeader  The header of the application.
   */
  public ConfirmAddRecipeDialog(Alert alert, Text headerText, Text contentText,
                                int width, int height, Header guiHeader) {
    super(alert, headerText, contentText, width, height);
    this.guiHeader = guiHeader;
    this.setStyle();
  }

  /**
   * Method to show the dialog box for adding a recipe to the shopping list.
   *
   * @param recipeName The name of the recipe to add to the shopping list.
   */
  public void showAddRecipeChoiceDialog(String recipeName) {

    // Set the style of the dialog box
    VBox vbox = new VBox();
    vbox.getChildren().addAll(getHeaderText(), getContentText());
    vbox.setSpacing(20);
    getAlert().getDialogPane().setContent(vbox);

    // Create buttons for the dialog box
    ButtonType addEntire = new ButtonType("Add Entire Recipe");
    ButtonType addRemaining = new ButtonType("Add Remaining Ingredients");
    ButtonType cancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

    // Add buttons to the dialog box
    getAlert().getButtonTypes().setAll(addEntire, addRemaining, cancel);

    // Get the buttons from the dialog box as buttons
    Button addEntireButton = (Button) getAlert().getDialogPane().lookupButton(addEntire);
    Button addRemainingButton = (Button) getAlert().getDialogPane().lookupButton(addRemaining);
    Button cancelButton = (Button) getAlert().getDialogPane().lookupButton(cancel);

    // Set style for buttons
    setPositiveButtonStyle(addEntireButton);
    setPositiveButtonStyle(addRemainingButton);
    setNegativeButtonStyle(cancelButton);

    // Show the dialog box and wait for the user to make a choice
    Optional<ButtonType> result = getAlert().showAndWait();

    // If the user chooses to add the entire recipe, add the recipe to the shopping list
    if (result.isPresent()) {
      if (result.get() == addEntire) {
        ListHandling.addSubtractFromList(getShoppingList(),
            getIngredientsFromRecipe(recipeName), 1);
        listToJson(getShoppingList(), "shopping_list");
        guiHeader.incrementShoppingListNotificationCount();
      } else if (result.get() == addRemaining) {
        updateShoppingList(recipeName);
        guiHeader.incrementShoppingListNotificationCount();
      }
    }
  }
}
