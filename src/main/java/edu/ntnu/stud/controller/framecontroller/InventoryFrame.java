package edu.ntnu.stud.controller.framecontroller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

/**
 * A frame for displaying the inventory. It extends the ItemListFrame and adds any specific
 * functionality for the inventory.
 *
 * @see ItemListFrame
 * @see JsonReadable
 */
public class InventoryFrame extends ItemListFrame implements JsonReadable {

  // Button
  @FXML
  @SuppressWarnings("unused")
  private Button inventoryToDbButton;
  @FXML
  @SuppressWarnings("unused")
  private Button inventoryClearButton;
  @FXML
  @SuppressWarnings("unused")
  private Button addIngredientButton;
  @FXML
  @SuppressWarnings("unused")
  private Button inventoryGetButton;

  /**
   * Takes in the file name for the .fxml file for the class extending the ItemListFrame class.
   */
  public InventoryFrame() {
    super("/fxml/inventoryFrame.fxml");
  }

  /**
   * Sets the event handler for the set inventory to database button.
   *
   * @param event The event handler for the button.
   */
  public void inventoryToDbButtonAction(EventHandler<ActionEvent> event) {
    inventoryToDbButton.setOnAction(event);
  }

  /**
   * Sets the event handler for the clear inventory button.
   *
   * @param event The event handler for the button.
   */
  public void inventoryClearButtonAction(EventHandler<ActionEvent> event) {
    inventoryClearButton.setOnAction(event);
  }

  /**
   * Sets the action for the add ingredient button.
   *
   * @param event The event handler for the button
   */
  public void setAddIngredientButtonAction(EventHandler<ActionEvent> event) {
    addIngredientButton.setOnAction(event);
  }

  /**
   * Sets the action for the add ingredient button.
   *
   * @param event The event handler for the button
   */
  public void inventoryGetButtonAction(EventHandler<ActionEvent> event) {
    inventoryGetButton.setOnAction(event);
  }
}
